﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor.Forms
{
    public enum Mode { Place, Select, Wireframe };

    public partial class ShortcutDialog : Form
    {
        private static List<string> shortcuts = new List<string>();
        private static List<string> descriptions = new List<string>();

        public static void AddShortcut(Mode mode, string shortcut, string desc)
        {
            shortcuts.Add(shortcut);
            descriptions.Add(desc);
        }

        public ShortcutDialog()
        {
            InitializeComponent();
            //foreach (string s in shortcuts)
                //ShortcutList.Items.Add(s);
            //foreach (string s in descriptions)
                //DescriptionList.Items.Add(s);
        }
    }
}
