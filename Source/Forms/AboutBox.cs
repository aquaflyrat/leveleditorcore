﻿using System;
using System.Reflection;
using System.Windows.Forms;
using PolyKnights.LevelEditor.Settings;

namespace PolyKnights.LevelEditor.Forms
{
    partial class AboutBox : Form
    {
        public AboutBox()
        {
            InitializeComponent();
            this.Text = String.Format("About {0}", About.Name);
            this.labelProductName.Text = About.Name;
            this.labelVersion.Text = About.Revision.Version + "        Build: " + About.Revision.Build;
            this.labelCopyright.Text = About.Author;
            this.labelCompanyName.Text = "F.A.R.T Team";
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Text = About.Desc + "\n\n" + About.License;
        }
        
    }
}
