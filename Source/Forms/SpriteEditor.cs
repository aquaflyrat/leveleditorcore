﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor.Forms
{
    public partial class SpriteEditor : Form
    {
        private Sprite sprite;

        public SpriteEditor(ref Sprite sprite)
        {
            InitializeComponent();
            this.sprite = sprite;

            if (sprite.Info == null)
                return;
            else
            {
                NameEntry.Text = sprite.Info.Name;
                IDEntry.Text = sprite.Info.ID;
            }
        }
        
        public Sprites.SpriteInfo GetInfo()
        {
            Sprites.SpriteInfo info = new Sprites.SpriteInfo();
            info.Name = NameEntry.Text;
            info.ID = IDEntry.Text;
            return info;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            sprite.Info = GetInfo();
        }
    }
}
