﻿namespace PolyKnights.LevelEditor
{
    partial class LevelEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LevelEditor));
            this.MenuBar = new System.Windows.Forms.MenuStrip();
            this.MenuFileAction = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuNewAction = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuLoadAction = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuSaveAction = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.OpenInNotepad = new System.Windows.Forms.ToolStripMenuItem();
            this.openInExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.FilesRecentToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.ClearRecentFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuPreferences = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuQuitAction = new System.Windows.Forms.ToolStripMenuItem();
            this.mapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMapOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.placeModeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ModePlaceToggleGridSnap = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuViewColourAction = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuViewGridAction = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuViewWireframe = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTogglePlace = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuViewToggleSelectMode = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WindowsObjectWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuRun = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuReportABug = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelpChangeLogAction = new System.Windows.Forms.ToolStripMenuItem();
            this.getLatestUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.triangleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pointPolyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiplePointsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainContainer = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.LevelTree = new System.Windows.Forms.TreeView();
            this.OutputLog = new System.Windows.Forms.TabControl();
            this.DebugTab = new System.Windows.Forms.TabPage();
            this.UserErrorLog = new System.Windows.Forms.ListBox();
            this.TabLog = new System.Windows.Forms.TabPage();
            this.KeyLogger = new System.Windows.Forms.TabControl();
            this.GeneralTab = new System.Windows.Forms.TabPage();
            this.GeneralLog = new System.Windows.Forms.ListBox();
            this.KeyTab = new System.Windows.Forms.TabPage();
            this.KeyLog = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.EditorAreaBMP = new System.Windows.Forms.PictureBox();
            this.ColorDialog = new System.Windows.Forms.ColorDialog();
            this.SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.NewFileIcon = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.UndoButton = new System.Windows.Forms.ToolStripButton();
            this.MenuRedo = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSelect = new System.Windows.Forms.ToolStripButton();
            this.ToolStripWireframe = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripGrid = new System.Windows.Forms.ToolStripButton();
            this.ColorToolStrip = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripPlay = new System.Windows.Forms.ToolStripButton();
            this.StopProcess = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainContainer)).BeginInit();
            this.MainContainer.Panel1.SuspendLayout();
            this.MainContainer.Panel2.SuspendLayout();
            this.MainContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.OutputLog.SuspendLayout();
            this.DebugTab.SuspendLayout();
            this.TabLog.SuspendLayout();
            this.KeyLogger.SuspendLayout();
            this.GeneralTab.SuspendLayout();
            this.KeyTab.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditorAreaBMP)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuBar
            // 
            this.MenuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuFileAction,
            this.mapToolStripMenuItem,
            this.modeToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.runToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.insertToolStripMenuItem});
            this.MenuBar.Location = new System.Drawing.Point(0, 0);
            this.MenuBar.Name = "MenuBar";
            this.MenuBar.Size = new System.Drawing.Size(1038, 24);
            this.MenuBar.TabIndex = 0;
            this.MenuBar.Text = "menuStrip1";
            // 
            // MenuFileAction
            // 
            this.MenuFileAction.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuNewAction,
            this.MenuLoadAction,
            this.MenuSaveAction,
            this.toolStripSeparator1,
            this.OpenInNotepad,
            this.openInExplorerToolStripMenuItem,
            this.toolStripSeparator7,
            this.exportToolStripMenuItem,
            this.toolStripSeparator9,
            this.FilesRecentToolStrip,
            this.ClearRecentFiles,
            this.toolStripSeparator2,
            this.MenuPreferences,
            this.MenuQuitAction});
            this.MenuFileAction.Name = "MenuFileAction";
            this.MenuFileAction.Size = new System.Drawing.Size(37, 20);
            this.MenuFileAction.Text = "File";
            // 
            // MenuNewAction
            // 
            this.MenuNewAction.Name = "MenuNewAction";
            this.MenuNewAction.Size = new System.Drawing.Size(166, 22);
            this.MenuNewAction.Text = "New";
            this.MenuNewAction.Click += new System.EventHandler(this.MenuNewAction_Click);
            // 
            // MenuLoadAction
            // 
            this.MenuLoadAction.Name = "MenuLoadAction";
            this.MenuLoadAction.Size = new System.Drawing.Size(166, 22);
            this.MenuLoadAction.Text = "Load";
            this.MenuLoadAction.Click += new System.EventHandler(this.MenuLoadAction_Click);
            // 
            // MenuSaveAction
            // 
            this.MenuSaveAction.Name = "MenuSaveAction";
            this.MenuSaveAction.Size = new System.Drawing.Size(166, 22);
            this.MenuSaveAction.Text = "Save";
            this.MenuSaveAction.Click += new System.EventHandler(this.MenuSaveAction_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(163, 6);
            // 
            // OpenInNotepad
            // 
            this.OpenInNotepad.Name = "OpenInNotepad";
            this.OpenInNotepad.Size = new System.Drawing.Size(166, 22);
            this.OpenInNotepad.Text = "Open in Notepad";
            this.OpenInNotepad.Click += new System.EventHandler(this.OpenInNotepad_Click);
            // 
            // openInExplorerToolStripMenuItem
            // 
            this.openInExplorerToolStripMenuItem.Name = "openInExplorerToolStripMenuItem";
            this.openInExplorerToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.openInExplorerToolStripMenuItem.Text = "Open In Explorer";
            this.openInExplorerToolStripMenuItem.Click += new System.EventHandler(this.openInExplorerToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(163, 6);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(163, 6);
            // 
            // FilesRecentToolStrip
            // 
            this.FilesRecentToolStrip.Name = "FilesRecentToolStrip";
            this.FilesRecentToolStrip.Size = new System.Drawing.Size(166, 22);
            this.FilesRecentToolStrip.Text = "Recent";
            // 
            // ClearRecentFiles
            // 
            this.ClearRecentFiles.Name = "ClearRecentFiles";
            this.ClearRecentFiles.Size = new System.Drawing.Size(166, 22);
            this.ClearRecentFiles.Text = "Clear Recent Files";
            this.ClearRecentFiles.Click += new System.EventHandler(this.ClearRecentFiles_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(163, 6);
            // 
            // MenuPreferences
            // 
            this.MenuPreferences.Name = "MenuPreferences";
            this.MenuPreferences.Size = new System.Drawing.Size(166, 22);
            this.MenuPreferences.Text = "Preferences";
            this.MenuPreferences.Click += new System.EventHandler(this.MenuPreferences_Click);
            // 
            // MenuQuitAction
            // 
            this.MenuQuitAction.Name = "MenuQuitAction";
            this.MenuQuitAction.Size = new System.Drawing.Size(166, 22);
            this.MenuQuitAction.Text = "Exit";
            this.MenuQuitAction.Click += new System.EventHandler(this.MenuQuitAction_Click);
            // 
            // mapToolStripMenuItem
            // 
            this.mapToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuMapOptions});
            this.mapToolStripMenuItem.Name = "mapToolStripMenuItem";
            this.mapToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.mapToolStripMenuItem.Text = "Map";
            // 
            // MenuMapOptions
            // 
            this.MenuMapOptions.Name = "MenuMapOptions";
            this.MenuMapOptions.Size = new System.Drawing.Size(116, 22);
            this.MenuMapOptions.Text = "Options";
            this.MenuMapOptions.Click += new System.EventHandler(this.MenuMapOptions_Click);
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.placeModeToolStripMenuItem1});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.modeToolStripMenuItem.Text = "Mode";
            // 
            // placeModeToolStripMenuItem1
            // 
            this.placeModeToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ModePlaceToggleGridSnap});
            this.placeModeToolStripMenuItem1.Name = "placeModeToolStripMenuItem1";
            this.placeModeToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.placeModeToolStripMenuItem1.Text = "Place Mode";
            // 
            // ModePlaceToggleGridSnap
            // 
            this.ModePlaceToggleGridSnap.Checked = true;
            this.ModePlaceToggleGridSnap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ModePlaceToggleGridSnap.Name = "ModePlaceToggleGridSnap";
            this.ModePlaceToggleGridSnap.Size = new System.Drawing.Size(180, 22);
            this.ModePlaceToggleGridSnap.Text = "Toggle Snap To Grid";
            this.ModePlaceToggleGridSnap.Click += new System.EventHandler(this.ModePlaceToggleGridSnap_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuViewColourAction,
            this.MenuViewGridAction,
            this.toolStripSeparator3,
            this.MenuViewWireframe,
            this.MenuTogglePlace,
            this.MenuViewToggleSelectMode,
            this.toolStripSeparator8,
            this.windowsToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // MenuViewColourAction
            // 
            this.MenuViewColourAction.Name = "MenuViewColourAction";
            this.MenuViewColourAction.Size = new System.Drawing.Size(178, 22);
            this.MenuViewColourAction.Text = "Colour";
            this.MenuViewColourAction.Click += new System.EventHandler(this.MenuViewColourAction_Click);
            // 
            // MenuViewGridAction
            // 
            this.MenuViewGridAction.Name = "MenuViewGridAction";
            this.MenuViewGridAction.Size = new System.Drawing.Size(178, 22);
            this.MenuViewGridAction.Text = "Toggle Grid";
            this.MenuViewGridAction.Click += new System.EventHandler(this.MenuViewGridAction_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(175, 6);
            // 
            // MenuViewWireframe
            // 
            this.MenuViewWireframe.Name = "MenuViewWireframe";
            this.MenuViewWireframe.Size = new System.Drawing.Size(178, 22);
            this.MenuViewWireframe.Text = "Toggle Wireframe";
            this.MenuViewWireframe.Click += new System.EventHandler(this.MenuViewWireframe_Click);
            // 
            // MenuTogglePlace
            // 
            this.MenuTogglePlace.Name = "MenuTogglePlace";
            this.MenuTogglePlace.Size = new System.Drawing.Size(178, 22);
            this.MenuTogglePlace.Text = "Toggle Place Mode";
            this.MenuTogglePlace.Click += new System.EventHandler(this.MenuTogglePlace_Click);
            // 
            // MenuViewToggleSelectMode
            // 
            this.MenuViewToggleSelectMode.Name = "MenuViewToggleSelectMode";
            this.MenuViewToggleSelectMode.Size = new System.Drawing.Size(178, 22);
            this.MenuViewToggleSelectMode.Text = "Toggle Select Mode";
            this.MenuViewToggleSelectMode.Click += new System.EventHandler(this.MenuViewToggleSelectMode_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(175, 6);
            // 
            // windowsToolStripMenuItem
            // 
            this.windowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WindowsObjectWindow});
            this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
            this.windowsToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.windowsToolStripMenuItem.Text = "Windows";
            // 
            // WindowsObjectWindow
            // 
            this.WindowsObjectWindow.Name = "WindowsObjectWindow";
            this.WindowsObjectWindow.Size = new System.Drawing.Size(156, 22);
            this.WindowsObjectWindow.Text = "Object Window";
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuRun,
            this.optionsToolStripMenuItem});
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.runToolStripMenuItem.Text = "Run";
            // 
            // MenuRun
            // 
            this.MenuRun.Name = "MenuRun";
            this.MenuRun.Size = new System.Drawing.Size(116, 22);
            this.MenuRun.Text = "Run";
            this.MenuRun.Click += new System.EventHandler(this.runToolStripMenuItem1_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuReportABug,
            this.MenuAbout,
            this.MenuHelpChangeLogAction,
            this.getLatestUpdateToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // MenuReportABug
            // 
            this.MenuReportABug.Name = "MenuReportABug";
            this.MenuReportABug.Size = new System.Drawing.Size(211, 22);
            this.MenuReportABug.Text = "Report a Bug/Suggestions";
            this.MenuReportABug.Click += new System.EventHandler(this.MenuReportABug_Click);
            // 
            // MenuAbout
            // 
            this.MenuAbout.Name = "MenuAbout";
            this.MenuAbout.Size = new System.Drawing.Size(211, 22);
            this.MenuAbout.Text = "About";
            this.MenuAbout.Click += new System.EventHandler(this.MenuAbout_Click_1);
            // 
            // MenuHelpChangeLogAction
            // 
            this.MenuHelpChangeLogAction.Name = "MenuHelpChangeLogAction";
            this.MenuHelpChangeLogAction.Size = new System.Drawing.Size(211, 22);
            this.MenuHelpChangeLogAction.Text = "Change Log";
            this.MenuHelpChangeLogAction.Click += new System.EventHandler(this.MenuHelpChangeLogAction_Click);
            // 
            // getLatestUpdateToolStripMenuItem
            // 
            this.getLatestUpdateToolStripMenuItem.Name = "getLatestUpdateToolStripMenuItem";
            this.getLatestUpdateToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.getLatestUpdateToolStripMenuItem.Text = "Get Latest Update";
            this.getLatestUpdateToolStripMenuItem.Click += new System.EventHandler(this.getLatestUpdateToolStripMenuItem_Click);
            // 
            // insertToolStripMenuItem
            // 
            this.insertToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.triangleToolStripMenuItem,
            this.quadToolStripMenuItem,
            this.pointPolyToolStripMenuItem,
            this.multiplePointsToolStripMenuItem});
            this.insertToolStripMenuItem.Name = "insertToolStripMenuItem";
            this.insertToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.insertToolStripMenuItem.Text = "Insert";
            // 
            // triangleToolStripMenuItem
            // 
            this.triangleToolStripMenuItem.Name = "triangleToolStripMenuItem";
            this.triangleToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.triangleToolStripMenuItem.Text = "Triangle";
            // 
            // quadToolStripMenuItem
            // 
            this.quadToolStripMenuItem.Name = "quadToolStripMenuItem";
            this.quadToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.quadToolStripMenuItem.Text = "Quad";
            // 
            // pointPolyToolStripMenuItem
            // 
            this.pointPolyToolStripMenuItem.Name = "pointPolyToolStripMenuItem";
            this.pointPolyToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.pointPolyToolStripMenuItem.Text = "3 Point Poly";
            // 
            // multiplePointsToolStripMenuItem
            // 
            this.multiplePointsToolStripMenuItem.Name = "multiplePointsToolStripMenuItem";
            this.multiplePointsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.multiplePointsToolStripMenuItem.Text = "Multiple Points";
            // 
            // MainContainer
            // 
            this.MainContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.MainContainer.Location = new System.Drawing.Point(0, 24);
            this.MainContainer.Name = "MainContainer";
            // 
            // MainContainer.Panel1
            // 
            this.MainContainer.Panel1.Controls.Add(this.splitContainer1);
            this.MainContainer.Panel1.SizeChanged += new System.EventHandler(this.MainContainer_Panel1_SizeChanged);
            // 
            // MainContainer.Panel2
            // 
            this.MainContainer.Panel2.Controls.Add(this.panel1);
            this.MainContainer.Size = new System.Drawing.Size(1038, 616);
            this.MainContainer.SplitterDistance = 385;
            this.MainContainer.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 41);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.LevelTree);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.OutputLog);
            this.splitContainer1.Size = new System.Drawing.Size(383, 573);
            this.splitContainer1.SplitterDistance = 260;
            this.splitContainer1.TabIndex = 1;
            // 
            // LevelTree
            // 
            this.LevelTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LevelTree.Location = new System.Drawing.Point(0, 0);
            this.LevelTree.Name = "LevelTree";
            this.LevelTree.Size = new System.Drawing.Size(383, 260);
            this.LevelTree.TabIndex = 0;
            // 
            // OutputLog
            // 
            this.OutputLog.Controls.Add(this.DebugTab);
            this.OutputLog.Controls.Add(this.TabLog);
            this.OutputLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OutputLog.Location = new System.Drawing.Point(0, 0);
            this.OutputLog.Name = "OutputLog";
            this.OutputLog.SelectedIndex = 0;
            this.OutputLog.Size = new System.Drawing.Size(383, 309);
            this.OutputLog.TabIndex = 1;
            // 
            // DebugTab
            // 
            this.DebugTab.Controls.Add(this.UserErrorLog);
            this.DebugTab.Location = new System.Drawing.Point(4, 22);
            this.DebugTab.Name = "DebugTab";
            this.DebugTab.Padding = new System.Windows.Forms.Padding(3);
            this.DebugTab.Size = new System.Drawing.Size(375, 283);
            this.DebugTab.TabIndex = 0;
            this.DebugTab.Text = "Output";
            this.DebugTab.UseVisualStyleBackColor = true;
            // 
            // UserErrorLog
            // 
            this.UserErrorLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UserErrorLog.FormattingEnabled = true;
            this.UserErrorLog.Location = new System.Drawing.Point(3, 3);
            this.UserErrorLog.Name = "UserErrorLog";
            this.UserErrorLog.Size = new System.Drawing.Size(369, 277);
            this.UserErrorLog.TabIndex = 0;
            // 
            // TabLog
            // 
            this.TabLog.Controls.Add(this.KeyLogger);
            this.TabLog.Location = new System.Drawing.Point(4, 22);
            this.TabLog.Name = "TabLog";
            this.TabLog.Padding = new System.Windows.Forms.Padding(3);
            this.TabLog.Size = new System.Drawing.Size(375, 283);
            this.TabLog.TabIndex = 1;
            this.TabLog.Text = "Debug";
            this.TabLog.UseVisualStyleBackColor = true;
            // 
            // KeyLogger
            // 
            this.KeyLogger.Controls.Add(this.GeneralTab);
            this.KeyLogger.Controls.Add(this.KeyTab);
            this.KeyLogger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KeyLogger.Location = new System.Drawing.Point(3, 3);
            this.KeyLogger.Name = "KeyLogger";
            this.KeyLogger.SelectedIndex = 0;
            this.KeyLogger.Size = new System.Drawing.Size(369, 277);
            this.KeyLogger.TabIndex = 1;
            // 
            // GeneralTab
            // 
            this.GeneralTab.Controls.Add(this.GeneralLog);
            this.GeneralTab.Location = new System.Drawing.Point(4, 22);
            this.GeneralTab.Name = "GeneralTab";
            this.GeneralTab.Padding = new System.Windows.Forms.Padding(3);
            this.GeneralTab.Size = new System.Drawing.Size(361, 251);
            this.GeneralTab.TabIndex = 0;
            this.GeneralTab.Text = "General";
            this.GeneralTab.UseVisualStyleBackColor = true;
            // 
            // GeneralLog
            // 
            this.GeneralLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralLog.FormattingEnabled = true;
            this.GeneralLog.Location = new System.Drawing.Point(3, 3);
            this.GeneralLog.Name = "GeneralLog";
            this.GeneralLog.Size = new System.Drawing.Size(355, 245);
            this.GeneralLog.TabIndex = 0;
            // 
            // KeyTab
            // 
            this.KeyTab.Controls.Add(this.KeyLog);
            this.KeyTab.Location = new System.Drawing.Point(4, 22);
            this.KeyTab.Name = "KeyTab";
            this.KeyTab.Padding = new System.Windows.Forms.Padding(3);
            this.KeyTab.Size = new System.Drawing.Size(361, 251);
            this.KeyTab.TabIndex = 1;
            this.KeyTab.Text = "Key Logger";
            this.KeyTab.UseVisualStyleBackColor = true;
            // 
            // KeyLog
            // 
            this.KeyLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KeyLog.FormattingEnabled = true;
            this.KeyLog.Location = new System.Drawing.Point(3, 3);
            this.KeyLog.Name = "KeyLog";
            this.KeyLog.Size = new System.Drawing.Size(355, 245);
            this.KeyLog.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.EditorAreaBMP);
            this.panel1.Location = new System.Drawing.Point(3, 41);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(645, 574);
            this.panel1.TabIndex = 1;
            // 
            // EditorAreaBMP
            // 
            this.EditorAreaBMP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditorAreaBMP.Location = new System.Drawing.Point(0, 0);
            this.EditorAreaBMP.Name = "EditorAreaBMP";
            this.EditorAreaBMP.Size = new System.Drawing.Size(645, 574);
            this.EditorAreaBMP.TabIndex = 0;
            this.EditorAreaBMP.TabStop = false;
            this.EditorAreaBMP.Click += new System.EventHandler(this.EditorAreaBMP_Click);
            this.EditorAreaBMP.Paint += new System.Windows.Forms.PaintEventHandler(this.EditorAreaBMP_Paint);
            this.EditorAreaBMP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EditorAreaBMP_MouseDown);
            this.EditorAreaBMP.MouseMove += new System.Windows.Forms.MouseEventHandler(this.EditorAreaBMP_MouseMove);
            this.EditorAreaBMP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.EditorAreaBMP_MouseUp);
            // 
            // ColorDialog
            // 
            this.ColorDialog.FullOpen = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewFileIcon,
            this.ToolStripSave,
            this.ToolStripOpen,
            this.toolStripSeparator4,
            this.UndoButton,
            this.MenuRedo,
            this.toolStripSeparator10,
            this.ToolStripSelect,
            this.ToolStripWireframe,
            this.toolStripButton1,
            this.toolStripSeparator5,
            this.ToolStripGrid,
            this.ColorToolStrip,
            this.toolStripSeparator6,
            this.ToolStripPlay,
            this.StopProcess});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.MaximumSize = new System.Drawing.Size(0, 56);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1038, 39);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "ToolStrip";
            // 
            // NewFileIcon
            // 
            this.NewFileIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NewFileIcon.Image = ((System.Drawing.Image)(resources.GetObject("NewFileIcon.Image")));
            this.NewFileIcon.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.NewFileIcon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewFileIcon.Name = "NewFileIcon";
            this.NewFileIcon.Size = new System.Drawing.Size(36, 36);
            this.NewFileIcon.Text = "New File";
            this.NewFileIcon.Click += new System.EventHandler(this.NewFileIcon_Click);
            // 
            // ToolStripSave
            // 
            this.ToolStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripSave.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripSave.Image")));
            this.ToolStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripSave.Name = "ToolStripSave";
            this.ToolStripSave.Size = new System.Drawing.Size(36, 36);
            this.ToolStripSave.Text = "Save File";
            this.ToolStripSave.Click += new System.EventHandler(this.ToolStripSave_Click);
            // 
            // ToolStripOpen
            // 
            this.ToolStripOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripOpen.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripOpen.Image")));
            this.ToolStripOpen.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripOpen.Name = "ToolStripOpen";
            this.ToolStripOpen.Size = new System.Drawing.Size(36, 36);
            this.ToolStripOpen.Text = "Open File";
            this.ToolStripOpen.Click += new System.EventHandler(this.ToolStripOpen_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // UndoButton
            // 
            this.UndoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.UndoButton.Image = ((System.Drawing.Image)(resources.GetObject("UndoButton.Image")));
            this.UndoButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.UndoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UndoButton.Name = "UndoButton";
            this.UndoButton.Size = new System.Drawing.Size(36, 36);
            this.UndoButton.Text = "toolStripButton2";
            this.UndoButton.Click += new System.EventHandler(this.UndoButton_Click);
            // 
            // MenuRedo
            // 
            this.MenuRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MenuRedo.Image = ((System.Drawing.Image)(resources.GetObject("MenuRedo.Image")));
            this.MenuRedo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MenuRedo.Name = "MenuRedo";
            this.MenuRedo.Size = new System.Drawing.Size(36, 36);
            this.MenuRedo.Text = "toolStripButton2";
            this.MenuRedo.Click += new System.EventHandler(this.MenuRedo_Click);
            // 
            // ToolStripSelect
            // 
            this.ToolStripSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripSelect.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripSelect.Image")));
            this.ToolStripSelect.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripSelect.Name = "ToolStripSelect";
            this.ToolStripSelect.Size = new System.Drawing.Size(36, 36);
            this.ToolStripSelect.Text = "Select Mode";
            this.ToolStripSelect.Click += new System.EventHandler(this.ToolStripSelect_Click);
            // 
            // ToolStripWireframe
            // 
            this.ToolStripWireframe.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripWireframe.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripWireframe.Image")));
            this.ToolStripWireframe.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripWireframe.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripWireframe.Name = "ToolStripWireframe";
            this.ToolStripWireframe.Size = new System.Drawing.Size(36, 36);
            this.ToolStripWireframe.Text = "Wireframe Mode";
            this.ToolStripWireframe.Click += new System.EventHandler(this.ToolStripWireframe_Click_1);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "Place Mode";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 39);
            // 
            // ToolStripGrid
            // 
            this.ToolStripGrid.Checked = true;
            this.ToolStripGrid.CheckOnClick = true;
            this.ToolStripGrid.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ToolStripGrid.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripGrid.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripGrid.Image")));
            this.ToolStripGrid.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripGrid.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripGrid.Name = "ToolStripGrid";
            this.ToolStripGrid.Size = new System.Drawing.Size(36, 36);
            this.ToolStripGrid.Text = "Toggle Grid";
            this.ToolStripGrid.Click += new System.EventHandler(this.ToolStripGrid_Click);
            // 
            // ColorToolStrip
            // 
            this.ColorToolStrip.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ColorToolStrip.Image = ((System.Drawing.Image)(resources.GetObject("ColorToolStrip.Image")));
            this.ColorToolStrip.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ColorToolStrip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ColorToolStrip.Name = "ColorToolStrip";
            this.ColorToolStrip.Size = new System.Drawing.Size(36, 36);
            this.ColorToolStrip.Text = "Color Dialog";
            this.ColorToolStrip.Click += new System.EventHandler(this.ColorToolStrip_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 39);
            // 
            // ToolStripPlay
            // 
            this.ToolStripPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripPlay.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripPlay.Image")));
            this.ToolStripPlay.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripPlay.Name = "ToolStripPlay";
            this.ToolStripPlay.Size = new System.Drawing.Size(36, 36);
            this.ToolStripPlay.Text = "Run";
            this.ToolStripPlay.Click += new System.EventHandler(this.ToolStripPlay_Click);
            // 
            // StopProcess
            // 
            this.StopProcess.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StopProcess.Image = ((System.Drawing.Image)(resources.GetObject("StopProcess.Image")));
            this.StopProcess.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.StopProcess.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StopProcess.Name = "StopProcess";
            this.StopProcess.Size = new System.Drawing.Size(36, 36);
            this.StopProcess.Text = "toolStripButton2";
            this.StopProcess.Click += new System.EventHandler(this.StopProcess_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 39);
            // 
            // LevelEditor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1038, 640);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.MainContainer);
            this.Controls.Add(this.MenuBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.MainMenuStrip = this.MenuBar;
            this.Name = "LevelEditor";
            this.RightToLeftLayout = true;
            this.Text = "F.A.R.T Editor";
            this.Deactivate += new System.EventHandler(this.LevelEditor_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LevelEditor_FormClosing);
            this.Load += new System.EventHandler(this.LevelEditor_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.LevelEditor_KeyUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LevelEditor_MouseDown);
            this.MenuBar.ResumeLayout(false);
            this.MenuBar.PerformLayout();
            this.MainContainer.Panel1.ResumeLayout(false);
            this.MainContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainContainer)).EndInit();
            this.MainContainer.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.OutputLog.ResumeLayout(false);
            this.DebugTab.ResumeLayout(false);
            this.TabLog.ResumeLayout(false);
            this.KeyLogger.ResumeLayout(false);
            this.GeneralTab.ResumeLayout(false);
            this.KeyTab.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EditorAreaBMP)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuBar;
        private System.Windows.Forms.ToolStripMenuItem MenuFileAction;
        private System.Windows.Forms.ToolStripMenuItem MenuQuitAction;
        private System.Windows.Forms.SplitContainer MainContainer;
        private System.Windows.Forms.TreeView LevelTree;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripMenuItem MenuSaveAction;
        private System.Windows.Forms.ColorDialog ColorDialog;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuViewColourAction;
        private System.Windows.Forms.ToolStripMenuItem MenuViewGridAction;
        private System.Windows.Forms.ToolStripMenuItem mapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuReportABug;
        private System.Windows.Forms.ToolStripMenuItem MenuAbout;
        private System.Windows.Forms.TabControl OutputLog;
        private System.Windows.Forms.TabPage DebugTab;
        private System.Windows.Forms.TabPage TabLog;
        private System.Windows.Forms.ListBox UserErrorLog;
        private System.Windows.Forms.ToolStripMenuItem MenuMapOptions;
        private System.Windows.Forms.ToolStripMenuItem MenuViewWireframe;
        private System.Windows.Forms.ToolStripMenuItem MenuTogglePlace;
        private System.Windows.Forms.ToolStripMenuItem MenuViewToggleSelectMode;
        private System.Windows.Forms.ToolStripMenuItem MenuHelpChangeLogAction;
        private System.Windows.Forms.SaveFileDialog SaveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem MenuLoadAction;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog;
        private System.Windows.Forms.ToolStripMenuItem MenuNewAction;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.TabControl KeyLogger;
        private System.Windows.Forms.TabPage GeneralTab;
        private System.Windows.Forms.ListBox GeneralLog;
        private System.Windows.Forms.TabPage KeyTab;
        private System.Windows.Forms.PictureBox EditorAreaBMP;
        private System.Windows.Forms.ListBox KeyLog;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ToolStripGrid;
        private System.Windows.Forms.ToolStripButton ToolStripOpen;
        private System.Windows.Forms.ToolStripButton ToolStripSave;
        private System.Windows.Forms.ToolStripButton NewFileIcon;
        private System.Windows.Forms.ToolStripButton ToolStripSelect;
        private System.Windows.Forms.ToolStripButton ToolStripWireframe;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton ColorToolStrip;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripButton ToolStripPlay;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FilesRecentToolStrip;
        private System.Windows.Forms.ToolStripMenuItem MenuRun;
        private System.Windows.Forms.ToolStripMenuItem OpenInNotepad;
        private System.Windows.Forms.ToolStripMenuItem openInExplorerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton StopProcess;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WindowsObjectWindow;
        private System.Windows.Forms.ToolStripButton UndoButton;
        private System.Windows.Forms.ToolStripMenuItem ClearRecentFiles;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem placeModeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ModePlaceToggleGridSnap;
        private System.Windows.Forms.ToolStripButton MenuRedo;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem getLatestUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuPreferences;
        private System.Windows.Forms.ToolStripMenuItem insertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem triangleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pointPolyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiplePointsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
    }
}

