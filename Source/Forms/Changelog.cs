﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor.Forms
{
    public partial class Changelog : Form
    {
        public void Deselect()
        {
            TextBox.DeselectAll();
        }
        public Changelog()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Text = "Change Log";
            TextBox.Text = Properties.Resources.Changelog.ToString();
        }
    }
}
