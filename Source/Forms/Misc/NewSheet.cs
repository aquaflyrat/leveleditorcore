﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PolyKnights.LevelEditor.Toolbox.Extensions;

namespace PolyKnights.LevelEditor.Forms.Misc
{
    public partial class NewSheet : Form
    {
        public string FilePath = "";

        public NewSheet()
        {
            InitializeComponent();

            TopMost = true;
            string ext = "png";
            FileNameDialog.Filter = ext.ToUpper() + " files (*." + ext + ") | *." + ext;
            FileNameDialog.DefaultExt = "." + ext;
            FileNameDialog.AddExtension = true;
        }

        private void NameBrowseButton_Click(object sender, EventArgs e)
        {
            DialogResult result = FileNameDialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            NameTextBox.Text = FileNameDialog.FileName;
            FilePath = NameTextBox.Text;
        }

        private void CreateLevelButton_Click(object sender, EventArgs e)
        {
            if (NameTextBox.Text.IsEmpty()) return;
            BitmapUtil.BitmapCreator creator = new BitmapUtil.BitmapCreator();
            creator.CreateImage(NameTextBox.Text,
                (int) MapWidth.Value, (int) MapHeight.Value,
                (int) TileWidth.Value, (int) TileHeight.Value);
            Close();
        }
    }
}
