﻿namespace PolyKnights.LevelEditor.Forms.Misc
{
    partial class NewSheet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewSheet));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CreateLevelButton = new System.Windows.Forms.Button();
            this.NameBrowseButton = new System.Windows.Forms.Button();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TileWidth = new System.Windows.Forms.NumericUpDown();
            this.TileHeight = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.MapHeight = new System.Windows.Forms.NumericUpDown();
            this.MapWidth = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FileNameDialog = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TileWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TileHeight)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MapHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapWidth)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CreateLevelButton);
            this.groupBox1.Controls.Add(this.NameBrowseButton);
            this.groupBox1.Controls.Add(this.NameTextBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(339, 225);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "New Level";
            // 
            // CreateLevelButton
            // 
            this.CreateLevelButton.Location = new System.Drawing.Point(121, 196);
            this.CreateLevelButton.Name = "CreateLevelButton";
            this.CreateLevelButton.Size = new System.Drawing.Size(75, 23);
            this.CreateLevelButton.TabIndex = 5;
            this.CreateLevelButton.Text = "OK";
            this.CreateLevelButton.UseVisualStyleBackColor = true;
            this.CreateLevelButton.Click += new System.EventHandler(this.CreateLevelButton_Click);
            // 
            // NameBrowseButton
            // 
            this.NameBrowseButton.Location = new System.Drawing.Point(243, 54);
            this.NameBrowseButton.Name = "NameBrowseButton";
            this.NameBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.NameBrowseButton.TabIndex = 4;
            this.NameBrowseButton.Text = "Browse...";
            this.NameBrowseButton.UseVisualStyleBackColor = true;
            this.NameBrowseButton.Click += new System.EventHandler(this.NameBrowseButton_Click);
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(54, 31);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(264, 20);
            this.NameTextBox.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Name";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TileWidth);
            this.groupBox3.Controls.Add(this.TileHeight);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(167, 83);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(172, 100);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tile Size";
            // 
            // TileWidth
            // 
            this.TileWidth.Increment = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.TileWidth.Location = new System.Drawing.Point(50, 24);
            this.TileWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.TileWidth.Name = "TileWidth";
            this.TileWidth.Size = new System.Drawing.Size(52, 20);
            this.TileWidth.TabIndex = 9;
            this.TileWidth.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            // 
            // TileHeight
            // 
            this.TileHeight.Increment = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.TileHeight.Location = new System.Drawing.Point(50, 66);
            this.TileHeight.Name = "TileHeight";
            this.TileHeight.Size = new System.Drawing.Size(52, 20);
            this.TileHeight.TabIndex = 8;
            this.TileHeight.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(117, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Pixels";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(117, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Pixels";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Width";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Height";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.MapHeight);
            this.groupBox2.Controls.Add(this.MapWidth);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(6, 83);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(160, 100);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Map Size";
            // 
            // MapHeight
            // 
            this.MapHeight.Location = new System.Drawing.Point(44, 64);
            this.MapHeight.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.MapHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MapHeight.Name = "MapHeight";
            this.MapHeight.Size = new System.Drawing.Size(52, 20);
            this.MapHeight.TabIndex = 7;
            this.MapHeight.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // MapWidth
            // 
            this.MapWidth.Location = new System.Drawing.Point(44, 24);
            this.MapWidth.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.MapWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MapWidth.Name = "MapWidth";
            this.MapWidth.Size = new System.Drawing.Size(52, 20);
            this.MapWidth.TabIndex = 6;
            this.MapWidth.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(102, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Tiles";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(102, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tiles";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Height";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Width";
            // 
            // NewSheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 225);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewSheet";
            this.Text = "New Sheet";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TileWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TileHeight)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MapHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapWidth)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button NameBrowseButton;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.SaveFileDialog FileNameDialog;
        private System.Windows.Forms.Button CreateLevelButton;
        private System.Windows.Forms.NumericUpDown TileWidth;
        private System.Windows.Forms.NumericUpDown TileHeight;
        private System.Windows.Forms.NumericUpDown MapHeight;
        private System.Windows.Forms.NumericUpDown MapWidth;
    }
}