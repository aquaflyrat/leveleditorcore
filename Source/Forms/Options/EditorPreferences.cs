﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor.Forms
{
    public partial class EditorPreferences : Form
    {
        public EditorPreferences()
        {
            InitializeComponent();
            var group = new Settings.Settings().GetGroup("Editor Settings");
            showTopOnLoad.Checked = group.GetValue<bool>("OpenTopOnLoad");
        }

        private void save_Click(object sender, EventArgs e)
        {
            var group = new Settings.Settings().GetGroup("Editor Settings");
            group.SetValue("OpenTopOnLoad", showTopOnLoad.Checked);
            Settings.Settings.Save();
            Close();
        }
    }
}
