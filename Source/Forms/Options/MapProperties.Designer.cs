﻿namespace PolyKnights.LevelEditor.Forms
{
    partial class MapProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapProperties));
            this.MapSettingsTabControl = new System.Windows.Forms.TabControl();
            this.GridSettings = new System.Windows.Forms.TabPage();
            this.PointsLabel = new System.Windows.Forms.Label();
            this.PolysLabel = new System.Windows.Forms.Label();
            this.PointsY = new System.Windows.Forms.NumericUpDown();
            this.PointsX = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PolysY = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.PolysX = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GridColorGroupBox = new System.Windows.Forms.GroupBox();
            this.GridColorAlphaValue = new System.Windows.Forms.NumericUpDown();
            this.LabelColorAlpha = new System.Windows.Forms.Label();
            this.LabelGridColor = new System.Windows.Forms.Label();
            this.ChooseGridColorButton = new System.Windows.Forms.Button();
            this.GridHeightValue = new System.Windows.Forms.NumericUpDown();
            this.LabelGridHeight = new System.Windows.Forms.Label();
            this.LabelGridWidth = new System.Windows.Forms.Label();
            this.GridWidthValue = new System.Windows.Forms.NumericUpDown();
            this.MapOtherSettings = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.DefaultOption = new System.Windows.Forms.Button();
            this.GridColorDialog = new System.Windows.Forms.ColorDialog();
            this.MapSettingsTabControl.SuspendLayout();
            this.GridSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PointsY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PointsX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolysY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolysX)).BeginInit();
            this.GridColorGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridColorAlphaValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridHeightValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridWidthValue)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MapSettingsTabControl
            // 
            this.MapSettingsTabControl.Controls.Add(this.GridSettings);
            this.MapSettingsTabControl.Controls.Add(this.MapOtherSettings);
            this.MapSettingsTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MapSettingsTabControl.Location = new System.Drawing.Point(3, 3);
            this.MapSettingsTabControl.Name = "MapSettingsTabControl";
            this.MapSettingsTabControl.SelectedIndex = 0;
            this.MapSettingsTabControl.Size = new System.Drawing.Size(419, 218);
            this.MapSettingsTabControl.TabIndex = 0;
            // 
            // GridSettings
            // 
            this.GridSettings.Controls.Add(this.PointsLabel);
            this.GridSettings.Controls.Add(this.PolysLabel);
            this.GridSettings.Controls.Add(this.PointsY);
            this.GridSettings.Controls.Add(this.PointsX);
            this.GridSettings.Controls.Add(this.label6);
            this.GridSettings.Controls.Add(this.label5);
            this.GridSettings.Controls.Add(this.PolysY);
            this.GridSettings.Controls.Add(this.label4);
            this.GridSettings.Controls.Add(this.PolysX);
            this.GridSettings.Controls.Add(this.label3);
            this.GridSettings.Controls.Add(this.label2);
            this.GridSettings.Controls.Add(this.label1);
            this.GridSettings.Controls.Add(this.GridColorGroupBox);
            this.GridSettings.Controls.Add(this.GridHeightValue);
            this.GridSettings.Controls.Add(this.LabelGridHeight);
            this.GridSettings.Controls.Add(this.LabelGridWidth);
            this.GridSettings.Controls.Add(this.GridWidthValue);
            this.GridSettings.Location = new System.Drawing.Point(4, 22);
            this.GridSettings.Name = "GridSettings";
            this.GridSettings.Padding = new System.Windows.Forms.Padding(3);
            this.GridSettings.Size = new System.Drawing.Size(411, 192);
            this.GridSettings.TabIndex = 0;
            this.GridSettings.Text = "Grid";
            this.GridSettings.UseVisualStyleBackColor = true;
            // 
            // PointsLabel
            // 
            this.PointsLabel.AutoSize = true;
            this.PointsLabel.Location = new System.Drawing.Point(18, 100);
            this.PointsLabel.Name = "PointsLabel";
            this.PointsLabel.Size = new System.Drawing.Size(36, 13);
            this.PointsLabel.TabIndex = 19;
            this.PointsLabel.Text = "Points";
            // 
            // PolysLabel
            // 
            this.PolysLabel.AutoSize = true;
            this.PolysLabel.Location = new System.Drawing.Point(18, 77);
            this.PolysLabel.Name = "PolysLabel";
            this.PolysLabel.Size = new System.Drawing.Size(32, 13);
            this.PolysLabel.TabIndex = 18;
            this.PolysLabel.Text = "Polys";
            // 
            // PointsY
            // 
            this.PointsY.Increment = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.PointsY.Location = new System.Drawing.Point(285, 98);
            this.PointsY.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.PointsY.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.PointsY.Name = "PointsY";
            this.PointsY.Size = new System.Drawing.Size(120, 20);
            this.PointsY.TabIndex = 17;
            this.PointsY.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.PointsY.ValueChanged += new System.EventHandler(this.PointsY_ValueChanged);
            // 
            // PointsX
            // 
            this.PointsX.Increment = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.PointsX.Location = new System.Drawing.Point(103, 100);
            this.PointsX.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.PointsX.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.PointsX.Name = "PointsX";
            this.PointsX.Size = new System.Drawing.Size(120, 20);
            this.PointsX.TabIndex = 15;
            this.PointsX.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.PointsX.ValueChanged += new System.EventHandler(this.PointsX_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(245, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Y";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(65, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "X";
            // 
            // PolysY
            // 
            this.PolysY.Increment = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.PolysY.Location = new System.Drawing.Point(285, 70);
            this.PolysY.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.PolysY.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.PolysY.Name = "PolysY";
            this.PolysY.Size = new System.Drawing.Size(120, 20);
            this.PolysY.TabIndex = 13;
            this.PolysY.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.PolysY.ValueChanged += new System.EventHandler(this.SnapToYValue_ValueChanged_1);
            this.PolysY.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SnapToYValue_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(245, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Y";
            // 
            // PolysX
            // 
            this.PolysX.Increment = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.PolysX.Location = new System.Drawing.Point(103, 70);
            this.PolysX.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.PolysX.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.PolysX.Name = "PolysX";
            this.PolysX.Size = new System.Drawing.Size(120, 20);
            this.PolysX.TabIndex = 11;
            this.PolysX.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.PolysX.ValueChanged += new System.EventHandler(this.SnapToYValue_ValueChanged);
            this.PolysX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SnapToXValue_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(221, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Height";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Width";
            // 
            // GridColorGroupBox
            // 
            this.GridColorGroupBox.Controls.Add(this.GridColorAlphaValue);
            this.GridColorGroupBox.Controls.Add(this.LabelColorAlpha);
            this.GridColorGroupBox.Controls.Add(this.LabelGridColor);
            this.GridColorGroupBox.Controls.Add(this.ChooseGridColorButton);
            this.GridColorGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GridColorGroupBox.Location = new System.Drawing.Point(3, 116);
            this.GridColorGroupBox.Name = "GridColorGroupBox";
            this.GridColorGroupBox.Size = new System.Drawing.Size(405, 73);
            this.GridColorGroupBox.TabIndex = 7;
            this.GridColorGroupBox.TabStop = false;
            this.GridColorGroupBox.Text = "Grid Color";
            // 
            // GridColorAlphaValue
            // 
            this.GridColorAlphaValue.Location = new System.Drawing.Point(221, 48);
            this.GridColorAlphaValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.GridColorAlphaValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.GridColorAlphaValue.Name = "GridColorAlphaValue";
            this.GridColorAlphaValue.Size = new System.Drawing.Size(120, 20);
            this.GridColorAlphaValue.TabIndex = 8;
            this.GridColorAlphaValue.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.GridColorAlphaValue.ValueChanged += new System.EventHandler(this.GridColorAlphaValue_ValueChanged);
            this.GridColorAlphaValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridColorAlphaValue_KeyDown);
            // 
            // LabelColorAlpha
            // 
            this.LabelColorAlpha.AutoSize = true;
            this.LabelColorAlpha.Location = new System.Drawing.Point(15, 57);
            this.LabelColorAlpha.Name = "LabelColorAlpha";
            this.LabelColorAlpha.Size = new System.Drawing.Size(61, 13);
            this.LabelColorAlpha.TabIndex = 7;
            this.LabelColorAlpha.Text = "Color Alpha";
            // 
            // LabelGridColor
            // 
            this.LabelGridColor.AutoSize = true;
            this.LabelGridColor.Location = new System.Drawing.Point(15, 30);
            this.LabelGridColor.Name = "LabelGridColor";
            this.LabelGridColor.Size = new System.Drawing.Size(53, 13);
            this.LabelGridColor.TabIndex = 6;
            this.LabelGridColor.Text = "Grid Color";
            // 
            // ChooseGridColorButton
            // 
            this.ChooseGridColorButton.Location = new System.Drawing.Point(266, 19);
            this.ChooseGridColorButton.Name = "ChooseGridColorButton";
            this.ChooseGridColorButton.Size = new System.Drawing.Size(75, 23);
            this.ChooseGridColorButton.TabIndex = 5;
            this.ChooseGridColorButton.Text = "Choose...";
            this.ChooseGridColorButton.UseVisualStyleBackColor = true;
            this.ChooseGridColorButton.Click += new System.EventHandler(this.ChooseGridColorButton_Click);
            // 
            // GridHeightValue
            // 
            this.GridHeightValue.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.GridHeightValue.Location = new System.Drawing.Point(285, 26);
            this.GridHeightValue.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.GridHeightValue.Minimum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.GridHeightValue.Name = "GridHeightValue";
            this.GridHeightValue.Size = new System.Drawing.Size(120, 20);
            this.GridHeightValue.TabIndex = 3;
            this.GridHeightValue.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.GridHeightValue.ValueChanged += new System.EventHandler(this.GridHeightValue_ValueChanged);
            this.GridHeightValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridHeightValue_KeyDown);
            // 
            // LabelGridHeight
            // 
            this.LabelGridHeight.AutoSize = true;
            this.LabelGridHeight.Location = new System.Drawing.Point(6, 57);
            this.LabelGridHeight.Name = "LabelGridHeight";
            this.LabelGridHeight.Size = new System.Drawing.Size(48, 13);
            this.LabelGridHeight.TabIndex = 2;
            this.LabelGridHeight.Text = "Snap To";
            // 
            // LabelGridWidth
            // 
            this.LabelGridWidth.AutoSize = true;
            this.LabelGridWidth.Location = new System.Drawing.Point(5, 3);
            this.LabelGridWidth.Name = "LabelGridWidth";
            this.LabelGridWidth.Size = new System.Drawing.Size(49, 13);
            this.LabelGridWidth.TabIndex = 1;
            this.LabelGridWidth.Text = "Grid Size";
            // 
            // GridWidthValue
            // 
            this.GridWidthValue.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.GridWidthValue.Location = new System.Drawing.Point(85, 26);
            this.GridWidthValue.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.GridWidthValue.Minimum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.GridWidthValue.Name = "GridWidthValue";
            this.GridWidthValue.Size = new System.Drawing.Size(120, 20);
            this.GridWidthValue.TabIndex = 0;
            this.GridWidthValue.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.GridWidthValue.ValueChanged += new System.EventHandler(this.GridWidthValue_ValueChanged);
            this.GridWidthValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridWidthValue_KeyDown);
            // 
            // MapOtherSettings
            // 
            this.MapOtherSettings.Location = new System.Drawing.Point(4, 22);
            this.MapOtherSettings.Name = "MapOtherSettings";
            this.MapOtherSettings.Padding = new System.Windows.Forms.Padding(3);
            this.MapOtherSettings.Size = new System.Drawing.Size(411, 192);
            this.MapOtherSettings.TabIndex = 1;
            this.MapOtherSettings.Text = "Other";
            this.MapOtherSettings.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.11765F));
            this.tableLayoutPanel1.Controls.Add(this.MapSettingsTabControl, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(425, 292);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.DefaultOption);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 227);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(419, 62);
            this.panel1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(273, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // DefaultOption
            // 
            this.DefaultOption.Location = new System.Drawing.Point(107, 30);
            this.DefaultOption.Name = "DefaultOption";
            this.DefaultOption.Size = new System.Drawing.Size(75, 23);
            this.DefaultOption.TabIndex = 0;
            this.DefaultOption.Text = "Default";
            this.DefaultOption.UseVisualStyleBackColor = true;
            this.DefaultOption.Click += new System.EventHandler(this.DefaultOption_Click);
            // 
            // MapProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 292);
            this.Controls.Add(this.tableLayoutPanel1);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MapProperties";
            this.Text = "Map Properties";
            this.Load += new System.EventHandler(this.MapProperties_Load);
            this.MapSettingsTabControl.ResumeLayout(false);
            this.GridSettings.ResumeLayout(false);
            this.GridSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PointsY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PointsX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolysY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolysX)).EndInit();
            this.GridColorGroupBox.ResumeLayout(false);
            this.GridColorGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridColorAlphaValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridHeightValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridWidthValue)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MapSettingsTabControl;
        private System.Windows.Forms.TabPage GridSettings;
        private System.Windows.Forms.NumericUpDown GridHeightValue;
        private System.Windows.Forms.Label LabelGridHeight;
        private System.Windows.Forms.Label LabelGridWidth;
        private System.Windows.Forms.NumericUpDown GridWidthValue;
        private System.Windows.Forms.TabPage MapOtherSettings;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button DefaultOption;
        private System.Windows.Forms.Button ChooseGridColorButton;
        private System.Windows.Forms.ColorDialog GridColorDialog;
        private System.Windows.Forms.Label LabelGridColor;
        private System.Windows.Forms.GroupBox GridColorGroupBox;
        private System.Windows.Forms.NumericUpDown GridColorAlphaValue;
        private System.Windows.Forms.Label LabelColorAlpha;
        private System.Windows.Forms.NumericUpDown PolysY;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown PolysX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown PointsX;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown PointsY;
        private System.Windows.Forms.Label PointsLabel;
        private System.Windows.Forms.Label PolysLabel;
    }
}