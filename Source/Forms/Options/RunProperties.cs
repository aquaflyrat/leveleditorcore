﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor.Forms
{
    public partial class RunProperties : Form
    {
        public RunProperties()
        {
            Settings.Settings settings = new Settings.Settings();
            var group = settings.GetGroup("Run Properties");
            InitializeComponent();
            UpdatePath((string)group.GetValue("AppPath"));

        }
        private void UpdatePath(string path)
        {

        }

        private void FindAppPath_Click(object sender, EventArgs e)
        {
            Settings.Settings settings = new Settings.Settings();
            var group = settings.GetGroup("Run Properties");
            var result = ChooseFolder.ShowDialog();
            if(result == DialogResult.OK)
            {
                group.SetValue("AppPath", ChooseFolder.SelectedPath);
            }
            UpdatePath((string)group.GetValue("AppPath"));
            textBox1.Text = (string)group.GetValue("AppPath");
        }

        private void CommandLineArgs_TextChanged(object sender, EventArgs e)
        {
            Settings.Settings settings = new Settings.Settings();
            var group = settings.GetGroup("Run Properties");
            group.SetValue("CmdArgs", CommandLineArgs.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Settings.Settings settings = new Settings.Settings();
            var group = settings.GetGroup("Run Properties");
            group.SetValue("AppPath", textBox1.Text);
        }

        private void RunProperties_Load(object sender, EventArgs e)
        {
            Settings.Settings settings = new Settings.Settings();
            var group = settings.GetGroup("Run Properties");
            textBox1.Text = (string)group.GetValue("AppPath");
            CommandLineArgs.Text = (string)group.GetValue("CmdArgs");

            if ((bool)group.GetValue("RunWithDefaultArgs")) {
                WithLevelFileCheck.CheckState = CheckState.Checked;
            } else
            {
                WithLevelFileCheck.CheckState = CheckState.Unchecked;
            }

            CommandLineArgs.Enabled = (WithLevelFileCheck.CheckState != CheckState.Checked) ? true : false;

            group.SetValue("RunWithDefaultArgs", (WithLevelFileCheck.CheckState == CheckState.Checked) ? true : false);
        }


        private void SaveButton_Click(object sender, EventArgs e)
        {
            Settings.Settings.Save();
            Close();
        }

        private void WithLevelFileCheck_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Settings settings = new Settings.Settings();
            var group = settings.GetGroup("Run Properties");
            group.SetValue("RunWithDefaultArgs", (WithLevelFileCheck.CheckState == CheckState.Checked) ? true : false);
            CommandLineArgs.Enabled = !(WithLevelFileCheck.CheckState == CheckState.Checked);
        }
    }
}
