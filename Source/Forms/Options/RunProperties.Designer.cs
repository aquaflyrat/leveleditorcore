﻿namespace PolyKnights.LevelEditor.Forms
{
    partial class RunProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RunProperties));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.WithLevelFileCheck = new System.Windows.Forms.CheckBox();
            this.CommandLineArgs = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.PathDisplay = new System.Windows.Forms.Label();
            this.FindAppPath = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SaveButton = new System.Windows.Forms.Button();
            this.ChooseFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70.06802F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.93197F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(427, 198);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.PathDisplay);
            this.groupBox1.Controls.Add(this.FindAppPath);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(421, 132);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Application";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.WithLevelFileCheck);
            this.groupBox2.Controls.Add(this.CommandLineArgs);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(3, 66);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(415, 63);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Advanved";
            // 
            // WithLevelFileCheck
            // 
            this.WithLevelFileCheck.AutoSize = true;
            this.WithLevelFileCheck.Checked = true;
            this.WithLevelFileCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.WithLevelFileCheck.Location = new System.Drawing.Point(92, 40);
            this.WithLevelFileCheck.Name = "WithLevelFileCheck";
            this.WithLevelFileCheck.Size = new System.Drawing.Size(234, 17);
            this.WithLevelFileCheck.TabIndex = 7;
            this.WithLevelFileCheck.Text = "Run With Default Command Line Arguments";
            this.WithLevelFileCheck.UseVisualStyleBackColor = true;
            this.WithLevelFileCheck.CheckedChanged += new System.EventHandler(this.WithLevelFileCheck_CheckedChanged);
            // 
            // CommandLineArgs
            // 
            this.CommandLineArgs.Location = new System.Drawing.Point(214, 16);
            this.CommandLineArgs.Name = "CommandLineArgs";
            this.CommandLineArgs.Size = new System.Drawing.Size(166, 20);
            this.CommandLineArgs.TabIndex = 4;
            this.CommandLineArgs.TextChanged += new System.EventHandler(this.CommandLineArgs_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Command Line Args";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(117, 24);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(212, 20);
            this.textBox1.TabIndex = 5;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // PathDisplay
            // 
            this.PathDisplay.AutoSize = true;
            this.PathDisplay.Location = new System.Drawing.Point(114, 50);
            this.PathDisplay.Name = "PathDisplay";
            this.PathDisplay.Size = new System.Drawing.Size(0, 13);
            this.PathDisplay.TabIndex = 2;
            // 
            // FindAppPath
            // 
            this.FindAppPath.Location = new System.Drawing.Point(337, 24);
            this.FindAppPath.Name = "FindAppPath";
            this.FindAppPath.Size = new System.Drawing.Size(75, 23);
            this.FindAppPath.TabIndex = 1;
            this.FindAppPath.Text = "Find...";
            this.FindAppPath.UseVisualStyleBackColor = true;
            this.FindAppPath.Click += new System.EventHandler(this.FindAppPath_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Application Path";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.SaveButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 153);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(421, 42);
            this.panel1.TabIndex = 1;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(163, 10);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 0;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // RunProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 198);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RunProperties";
            this.Text = "Run Properties";
            this.Load += new System.EventHandler(this.RunProperties_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button FindAppPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog ChooseFolder;
        private System.Windows.Forms.Label PathDisplay;
        private System.Windows.Forms.TextBox CommandLineArgs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.CheckBox WithLevelFileCheck;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}