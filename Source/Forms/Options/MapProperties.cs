﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using PolyKnights.LevelEditor.Settings;

namespace PolyKnights.LevelEditor.Forms
{
    public partial class MapProperties : Form
    {
        public MapProperties()
        {
            InitializeComponent();
        }

        private void SetVisibleDefaultValues()
        {
            var settings = new Settings.Settings();
            var group = settings.GetGroup("Map Properties");
            GridColorAlphaValue.Value = (int) group.GetDefault("GridColorAlpha");
            GridWidthValue.Value = (int) group.GetDefault("GridWidth");
            GridHeightValue.Value = (int) group.GetDefault("GridHeight");
            PolysX.Value = (int) group.GetDefault("PlaceSnapX");
            PolysY.Value = (int) group.GetDefault("PlaceSnapY");
            PointsX.Value = (int) group.GetDefault("PointSnapX");
            PointsY.Value = (int) group.GetDefault("PointSnapY");
        }

        private void ChooseGridColorButton_Click(object sender, EventArgs e)
        {
            DialogResult result = GridColorDialog.ShowDialog();
            if(result == DialogResult.OK)
            {
                var settings = new Settings.Settings();
                settings.GetGroup("Map Properties").SetValue("GridColor", GridColorDialog.Color);
            }
        }

        private void GridWidthValue_ValueChanged(object sender, EventArgs e)
        {
            new Settings.Settings().GetGroup("Map Properties").SetValue("GridWidth", (int) GridWidthValue.Value);
        }

        private void GridHeightValue_ValueChanged(object sender, EventArgs e)
        {
            new Settings.Settings().GetGroup("Map Properties").SetValue("GridHeight", (int)GridHeightValue.Value);
        }
        
        private void DefaultOption_Click(object sender, EventArgs e)
        {
            var settings = new Settings.Settings();
            settings.GetGroup("Map Properties").ResetToDefault();
            SetVisibleDefaultValues();
        }

        private void GridColorAlphaValue_ValueChanged(object sender, EventArgs e)
        {
            if(GridColorAlphaValue.Value <= 255)
                new Settings.Settings().GetGroup("Map Properties").SetValue("GridColorAlpha", (int) GridColorAlphaValue.Value);
        }

        private void SuppressEnterSound(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                e.SuppressKeyPress = true;
        }

        private void GridWidthValue_KeyDown(object sender, KeyEventArgs e)
        {
            SuppressEnterSound(e);
        }
        private void GridHeightValue_KeyDown(object sender, KeyEventArgs e)
        {
            SuppressEnterSound(e);
        }
        private void GridColorAlphaValue_KeyDown(object sender, KeyEventArgs e)
        {
            SuppressEnterSound(e);
        }

        private void SnapToYValue_ValueChanged(object sender, EventArgs e)
        {
            new Settings.Settings().GetGroup("Map Properties").SetValue("PlaceSnapX", (int)PolysX.Value);
        }

        private void SnapToYValue_ValueChanged_1(object sender, EventArgs e)
        {
            new Settings.Settings().GetGroup("Map Properties").SetValue("PlaceSnapY", (int)PolysY.Value);
        }

        private void SnapToXValue_KeyDown(object sender, KeyEventArgs e)
        {
            SuppressEnterSound(e);
        }

        private void SnapToYValue_KeyDown(object sender, KeyEventArgs e)
        {
            SuppressEnterSound(e);
        }

        private void LoadVisibleSettings()
        {
            var group = new Settings.Settings().GetGroup("Map Properties");
            GridColorAlphaValue.Value = (int)group.GetValue("GridColorAlpha");
            GridWidthValue.Value = (int)group.GetValue("GridWidth");
            GridHeightValue.Value = (int)group.GetValue("GridHeight");
            PolysX.Value = (int)group.GetValue("PlaceSnapX");
            PolysY.Value = (int)group.GetValue("PlaceSnapY");
            PointsX.Value = (int)group.GetValue("PointSnapX");
            PointsY.Value = (int)group.GetValue("PointSnapY");

        }

        private void MapProperties_Load(object sender, EventArgs e)
        {
            LoadVisibleSettings();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Settings.Settings.Save();
            Close();
        }

        private void PointsX_ValueChanged(object sender, EventArgs e)
        {
            new Settings.Settings().GetGroup("Map Properties").SetValue("PointSnapX", (int)PointsX.Value);
        }

        private void PointsY_ValueChanged(object sender, EventArgs e)
        {
            new Settings.Settings().GetGroup("Map Properties").SetValue("PointSnapY", (int)PointsY.Value);
        }

    }
}
