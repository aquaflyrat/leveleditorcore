﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor.Forms
{
    public partial class ObjectWindow : OtherWindows.Dock
    {
        private Spritesheet sheet = null;
        private Rectangle selector = new Rectangle();
        private bool selecting = false;
        private Point selectPoint = new Point();
        private string editingPath = "";

        public ObjectWindow(ref ToolStripMenuItem item)
        {
            InitializeComponent();
            InitDock(ref item);

            PictureBox.Image = new Bitmap(PictureBox.Width, PictureBox.Height);

            string ext = "png";
            OpenSheetDialog.Filter = ext.ToUpper() + " files (*." + ext + ") | *." + ext;
            OpenSheetDialog.DefaultExt = "." + ext;
            OpenSheetDialog.AddExtension = true;
        }

        public override void Tick()
        {
            base.Refresh();
        }

        private void PictureBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.Clear(Color.DarkGray);

            if(sheet != null)
            {
                g.DrawImage(sheet.Sheet, 0, 0);
            }

            for(int x = 0; x < PictureBox.Width; x += 32)
            {
                g.DrawLine(Pens.Black, x, 0, x, PictureBox.Height);
            }
            for (int y = 0; y < PictureBox.Height; y += 32)
            {
                g.DrawLine(Pens.Black, 0, y, PictureBox.Width, y);
            }

            g.FillRectangle(new SolidBrush(Color.FromArgb(155, 58, 135, 171)), selector);
            g.DrawRectangle(Pens.LightBlue, selector);
        }


        private void MenuClearCurrent_Click(object sender, EventArgs e)
        {
            sheet = null;
        }

        private void PictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (sheet == null)
                return;

            selector = new Rectangle();
            Sprite sprite = sheet.GetSprite(e.Location);

            if (sprite.Info == null)
            {
                Forms.SpriteEditor spriteEditor = new Forms.SpriteEditor(ref sprite);
                spriteEditor.Show(this);
            }

            Level.ImagePlace = sprite;
            PlaceMode.Placing = WhatToPlace.Image;
        }

        private void ObjectWindow_MouseUp(object sender, MouseEventArgs e)
        {
        }

        private Point mousePos = new Point();
        private void PictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            int mx = e.Location.X;
            int my = e.Location.Y;
            mousePos = e.Location;
            if (ModifierKeys.HasFlag(Keys.Shift)) {
                mx = (int)Math.Floor((double)e.Location.X / 32) * 32;
                my = (int)Math.Floor((double)e.Location.Y / 32) *32;
            }

            if(e.Button == MouseButtons.Left)
            {
                if (!selecting)
                {
                    selecting = true;
                    selectPoint.X = mx;
                    selectPoint.Y = my;
                }

                if (selecting)
                {
                    selector = new Rectangle(
                         Math.Min(selectPoint.X, mx),
                         Math.Min(selectPoint.Y, my),
                         Math.Abs(mx - selectPoint.X),
                         Math.Abs(my - selectPoint.Y));
                }
            }
        }

        private void PictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (sheet == null)
                return;

            selecting = false;
            if (selector.Width == 0 || selector.Height == 0)
                return;
           
            Level.ImagePlace = sheet.GetSprite(selector);
        }

        private void ObjectWindow_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 'e')
            {
                Sprite sprite = sheet.GetSprite(mousePos);
                new Forms.SpriteEditor(ref sprite);
            }
        }

        private void MenuEdit_Click(object sender, EventArgs e)
        {
            System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();
            info.FileName = "mspaint";
            info.Arguments = editingPath;
            System.Diagnostics.Process.Start(info);
        }

        private void FileLoadSheet_Click(object sender, EventArgs e)
        {
            DialogResult r = OpenSheetDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                sheet = new Spritesheet(OpenSheetDialog.FileName);
                editingPath = OpenSheetDialog.FileName;
            }
        }

        private void FileNewSheet_Click(object sender, EventArgs e)
        {
            Misc.NewSheet newSheet = new Misc.NewSheet();
            newSheet.ShowDialog();
            editingPath = newSheet.FilePath;
        }
    }
}
