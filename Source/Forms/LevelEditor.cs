﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor
{
    public partial class LevelEditor : Form
    {
        private static ListBox DebugLog;
        private static ListBox UserLog;

        public static CommandManager UndoManager = new CommandManager();

        private EditorArea editorArea;
        private KeyboardShortcuts shortcuts = new KeyboardShortcuts();
        public static Toolbox.File CurrentFile = Toolbox.File.NewFile;
        private string title = "F.A.R.T Editor";
        private Point cursorPos = new Point(0, 0);

        private OtherWindows.OtherWindows other = new OtherWindows.OtherWindows();
        private UI.RecentFiles recentFiles = new UI.RecentFiles();
        private UI.ApplicationRunner appRunner = new UI.ApplicationRunner();

        public static void Unsave()
        {
            CurrentFile.IsSaved = false;
            if(!Program.Editor.Text.Contains("*"))
                Program.Editor.Text = Program.Editor.Text + " *";
        }

        private void Update(object sender, EventArgs e)
        {
            other.Update();            
            cursorPos = Cursor.Position;
            EditorAreaBMP.Refresh();
        }

        private void SetupLocaleSettings()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void SetupFileDialogs()
        {
            string ext = "pmap";
            SaveFileDialog.Filter = ext.ToUpper() + " files (*." + ext + ") | *." + ext;
            SaveFileDialog.DefaultExt = "." + ext;
            SaveFileDialog.AddExtension = true;
            OpenFileDialog.Filter = ext.ToUpper() + " files (*." + ext + ") | *." + ext;
            OpenFileDialog.DefaultExt = "." + ext;
            OpenFileDialog.AddExtension = true;
        }

        private void SetupWindowOptions()
        {
            this.KeyPreview = true;
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.UserPaint |
              ControlStyles.AllPaintingInWmPaint |
              ControlStyles.ResizeRedraw |
              ControlStyles.ContainerControl |
              ControlStyles.OptimizedDoubleBuffer |
              ControlStyles.SupportsTransparentBackColor
              , true);
            //WindowState = FormWindowState.Maximized;

            openInExplorerToolStripMenuItem.Enabled = false;
            OpenInNotepad.Enabled = false;
        }

        private void CheckCommandLineArgs()
        {
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                editorArea.Load(args[1]);
                title = args[1] + " | " + title;
                Text = title;

                CurrentFile = new Toolbox.File(args[1], true);
            }
        }

        private void LoadEditor()
        {
            DebugLog = this.GeneralLog;
            UserLog = this.UserErrorLog;
            Timer timer = new Timer();
            timer.Interval = 16;
            timer.Tick += new EventHandler(Update);

            LevelEditor.Debug("Loading Editor...");

            EditorAreaBMP.Image = new Bitmap(EditorAreaBMP.Width, EditorAreaBMP.Height);
            editorArea = new EditorArea(EditorAreaBMP.Image);
            Settings.AppSettings.Initalize();
            SetupFileDialogs();
            SetupCallbacks();
            SetupKeyboardShortcuts();
            SetupLocaleSettings();
            SetupOtherWindows();
            SetupWindowOptions();
            CheckCommandLineArgs();

            recentFiles.LoadAll(RecentFileClicked, ref FilesRecentToolStrip);

            OpenFile(FilesRecentToolStrip.DropDownItems[0].Text);

            timer.Start();
        }

        private void SetupOtherWindows()
        {
            other.Add(WindowsObjectWindow.Name, new Forms.ObjectWindow(ref WindowsObjectWindow));
        }
        
        public static void Debug(object o)
        {
            DebugLog.Items.Add(o);
        }
        public static void UserError(object o)
        {
            UserLog.Items.Add(o.ToString());
        }

        private void SwitchWireframe()
        {
            editorArea.EnableWireframeMode();
        }

        private void SetupKeyboardShortcuts()
        {
            shortcuts.AddShortcut(Keys.C, ShowColourDialog, "C", "Show Color dialog", ref MenuViewColourAction);
            shortcuts.AddShortcut(Keys.Control | Keys.S, Save, "Ctrl + S", "Save", ref MenuSaveAction);
            shortcuts.AddShortcut(Keys.Control | Keys.O, Open, "Ctrl + O", "Open", ref MenuLoadAction);
            shortcuts.AddShortcut(Keys.G, SwitchGrid, "G", "Toggle Grid", ref MenuViewGridAction);
            shortcuts.AddShortcut(Keys.O, ShowMapOptions, "O", "Show map properties",ref MenuMapOptions);
            shortcuts.AddShortcut(Keys.S, SwitchSelectMode, "S", "Toggle Select Mode",ref MenuViewToggleSelectMode);
            shortcuts.AddShortcut(Keys.W, SwitchWireframe, "W", "Toggle Wireframe Mode",ref MenuViewWireframe);
            shortcuts.AddShortcut(Keys.A, SwitchPlace, "A", "Toggle place Mode",ref MenuTogglePlace);
            shortcuts.AddShortcut(Keys.Control | Keys.N, NewFile, "Ctrl + N", "New File", ref MenuNewAction);
            shortcuts.AddShortcut(Keys.F9, RunEXEApp, "F9", "Run", ref MenuRun);
        }

        private void SetupCallbacks()
        {
            ResizeEnd += new EventHandler(LevelEditor_ResizeEnd);
            EditorAreaBMP.MouseDown += new MouseEventHandler(LevelEditor_MouseDown);
            EditorAreaBMP.MouseUp += new MouseEventHandler(LevelEditor_MouseReleased);
            EditorAreaBMP.MouseWheel += EditorAreaBMP_MouseWheel;
        }

        private void EditorAreaBMP_MouseWheel(object sender, MouseEventArgs e)
        {
            editorArea.MouseWheel(e);
        }

        private void SwitchGrid() { Components.GridComponent.GridEnabled = !Components.GridComponent.GridEnabled; }
        
        private void NewFile()
        {
            if (!LevelEditor.CurrentFile.IsSaved)
            {
                var auto = MessageBox.Show("Do you want to save?", "Save?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Asterisk);
                if (auto == DialogResult.Yes)
                {
                    Save();
                    editorArea.NewFile();
                    CurrentFile = Toolbox.File.NewFile;
                }
                else if (auto == DialogResult.No)
                {
                    editorArea.NewFile();
                    CurrentFile = Toolbox.File.NewFile;
                }
            }
            else {
                editorArea.NewFile();
                CurrentFile = Toolbox.File.NewFile;
            }
            title = "F.A.R.T Editor";
        }

        private bool Quit(bool onCloseCheck=false)
        {
            if (!LevelEditor.CurrentFile.IsSaved)
            {
                var auto = MessageBox.Show("Do you want to save?", "Save?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Asterisk);
                if (auto == DialogResult.Yes)
                {
                    OtherWindows.OtherWindows.ParentClosing = true;
                    Save();
                    Application.Exit();
                }
                else if (auto == DialogResult.No)
                {
                    OtherWindows.OtherWindows.ParentClosing = true;
                    Application.Exit();
                    return true;
                }
                else if (auto == DialogResult.Cancel)
                {
                    return false;
                }
            }
            else {
                OtherWindows.OtherWindows.ParentClosing = true;
                Application.Exit();
            }
            return true;
        }
        private void ResizeEditor()
        {
            EditorAreaBMP.Image = new Bitmap(Width - MainContainer.Panel1.Width, Height);
            EditorAreaBMP.Width = EditorAreaBMP.Image.Width;
            EditorAreaBMP.Height = EditorAreaBMP.Image.Height;

            editorArea.RefreshImage(EditorAreaBMP.Image);
        }
        private void ShowColourDialog()
        {
            DialogResult result = ColorDialog.ShowDialog();
            
            if (result == DialogResult.OK)
            {
                Level.CurrentColor = ColorDialog.Color;
            }
        }

        private void SwitchSelectMode()
        {
            editorArea.EnableSelectMode();
        }

        private void SwitchPlace()
        {
            editorArea.EnablePlaceMode();
        }

        private void ShowMapOptions()
        {
            Forms.MapProperties mapProperties = new Forms.MapProperties();
            mapProperties.Show(this);
        }

        private void Save()
        {
            if (CurrentFile == Toolbox.File.NewFile)
            {
                DialogResult result = SaveFileDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    editorArea.Save(SaveFileDialog.FileName);
                    title = SaveFileDialog.FileName + " | " + title;
                    Text = title;
                    CurrentFile = new Toolbox.File(SaveFileDialog.FileName, true);

                    var settings = new Settings.Settings();
                    var group = settings.GetGroup("Recent Files");
                    group.AddSetting(CurrentFile.FileName, CurrentFile.FileName);
                    Settings.Settings.Save();
                    foreach (ToolStripMenuItem item in FilesRecentToolStrip.DropDownItems)
                    {
                        if (item.Text == CurrentFile.FileName) return;
                    }

                    ToolStripMenuItem menuItem = new ToolStripMenuItem(CurrentFile.FileName);
                    menuItem.Click += RecentFileClicked;
                    FilesRecentToolStrip.DropDownItems.Add(menuItem);
                }
            }
            else
            {
                editorArea.Save(CurrentFile.FileName);
                CurrentFile.IsSaved = true;
                Text = title;
                
            }
        }

        private void OpenFile(string name)
        {
            LevelEditor.Debug("Opening file!");
            LevelTree.Nodes.Add(name);
            LevelTree.Nodes[0].Nodes.Add(name + "Hi!");
            editorArea.Load(name);
            title = "F.A.R.T Editor";
            title = name + " | " + title;
            Text = title;

            CurrentFile = new Toolbox.File(name, true);

            recentFiles.AddFile(RecentFileClicked, ref FilesRecentToolStrip, CurrentFile, name);
        }

        private void Open()
        {
            DialogResult result = OpenFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                OpenFile(OpenFileDialog.FileName);
            }
        }

        private void RecentFileClicked(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            OpenFile(item.Text);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            shortcuts.Process(keyData);
            Point point = Cursor.Position;
            point = EditorAreaBMP.PointToClient(point);

            if (keyData == (Keys.Control | Keys.Z))
                UndoManager.Undo();
            else if (keyData == (Keys.Control | Keys.Y))
                UndoManager.Redo();

            if (EditorAreaBMP.ClientRectangle.Contains(point))
            {
                editorArea.ProcessKeys(keyData);
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
        
        public LevelEditor() { InitializeComponent(); }
        private void MenuQuitAction_Click(object sender, EventArgs e) { Quit(); }
        private void LevelEditor_Load(object sender, EventArgs e) { LoadEditor(); }
        private void LevelEditor_ResizeEnd(object sender, EventArgs e) { ResizeEditor(); }
        private void LevelEditor_MouseReleased(object sender, EventArgs e) { editorArea.OnMouseUp(); }

        private void EditorAreaBMP_Click(object sender, EventArgs e)
        {

        }

        private void MenuSaveAction_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void MenuViewColourAction_Click(object sender, EventArgs e) { ShowColourDialog(); }
        private void MenuViewGridAction_Click(object sender, EventArgs e) { SwitchGrid(); }
        
        private void MenuReportABug_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(About.BugtrackerURL);
        }
        private void MenuAbout_Click_1(object sender, EventArgs e)
        {
            Forms.AboutBox about = new Forms.AboutBox();
            about.ShowDialog();
        }
        private void SelectModeAction_Click(object sender, EventArgs e)
        {
            SwitchSelectMode();
        }
        private void MainContainer_Panel1_SizeChanged(object sender, EventArgs e)
        {
            if (EditorAreaBMP != null && editorArea != null)
                ResizeEditor();
        }
        

        private void EditorAreaBMP_MouseMove(object sender, MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Middle)
            {
                editorArea.MiddleMouseDown(e);
            } else if(e.Button == MouseButtons.Left)
            {
                editorArea.OnMouseDown();
            }
        }
        private void MenuMapOptions_Click(object sender, EventArgs e)
        {
            ShowMapOptions();
        }
        private void LevelEditor_KeyUp(object sender, KeyEventArgs e)
        {
            editorArea.OnKeyUp(e);

            // Exclude just the ControlKey being logged.
            if (e.KeyData != Keys.ControlKey)
            {
                KeyLog.Items.Add("Pressed: " + e.KeyData.ToString());
            }
        }
        private void MenuViewWireframe_Click(object sender, EventArgs e)
        {
            SwitchWireframe();
        }
        private void MenuViewToggleSelectMode_Click(object sender, EventArgs e)
        {
            SwitchSelectMode();
        }
        private void MenuTogglePlace_Click(object sender, EventArgs e)
        {
            SwitchPlace();
        }
        private void MenuHelpChangeLogAction_Click(object sender, EventArgs e)
        {
            Forms.Changelog changelog = new Forms.Changelog();
            changelog.Show();
            changelog.Deselect();
        }
        private void MenuLoadAction_Click(object sender, EventArgs e)
        {
            Open();   
        }

        private void LevelEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing)
                return;

            if(!Quit())
            {
                e.Cancel = true;
            }

        }

        private void MenuNewAction_Click(object sender, EventArgs e)
        {
            NewFile();
        }

        private void MenuKeyboardShortcuts_Click(object sender, EventArgs e)
        {
            Forms.ShortcutDialog dialog = new Forms.ShortcutDialog();
            dialog.Show();
        }

        private void EditorAreaBMP_Paint(object sender, PaintEventArgs e)
        {
            cursorPos = EditorAreaBMP.PointToClient(cursorPos);
            editorArea.Update(cursorPos.X, cursorPos.Y, ModifierKeys);
        }

        private void ToolStripOpen_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void ToolStripGrid_Click(object sender, EventArgs e)
        {
            SwitchGrid();
        }

        private void ToolStripSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void NewFileIcon_Click(object sender, EventArgs e)
        {
            NewFile();
        }

        private void ToolStripSelect_Click(object sender, EventArgs e)
        {
            SwitchSelectMode();
        }

        

        private void ColorToolStrip_Click(object sender, EventArgs e)
        {
            ShowColourDialog();
        }

        private void ToolStripWireframe_Click_1(object sender, EventArgs e)
        {
            SwitchWireframe();
        }

        private void EditorAreaBMP_MouseUp(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Middle)
            {
                editorArea.MiddleMouseUp();
            } 
        }

        // Place Toolstrip button
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SwitchPlace();
        }

        private void EditorAreaBMP_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
                editorArea.OnClick();
            if (e.Button == MouseButtons.Middle)
                editorArea.MiddleMouseClick();
        }

        private void RunEXEApp()
        {
            appRunner.Start(this, CurrentFile);
        }

        private void ToolStripPlay_Click(object sender, EventArgs e)
        {
            RunEXEApp();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.RunProperties run = new Forms.RunProperties();
            run.Show(this);
        }

        private void runToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            RunEXEApp();
        }

        private void OpenInNotepad_Click(object sender, EventArgs e)
        {
            if (CurrentFile == Toolbox.File.NewFile)
                return;

            var startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.FileName = "notepad";
            startInfo.Arguments = CurrentFile.FileName;
            System.Diagnostics.Process.Start(startInfo);
        }

        private void openInExplorerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CurrentFile == Toolbox.File.NewFile)
                return;

            System.Diagnostics.Process.Start(System.IO.Path.GetDirectoryName(CurrentFile.FileName));
        }

        private void StopProcess_Click(object sender, EventArgs e)
        {
            appRunner.Stop();
        }
        
        private void LevelEditor_MouseDown(object sender, MouseEventArgs e)
        {
            editorArea.OnMouseDown();
        }

        private void LevelEditor_Deactivate(object sender, EventArgs e)
        {
            Focus();
        }

        private void UndoButton_Click(object sender, EventArgs e)
        {
            UndoManager.Undo();
        }

        private void ClearRecentFiles_Click(object sender, EventArgs e)
        {
            var group = new Settings.Settings().GetGroup("Recent Files");
            group.Clear();
            Settings.Settings.Save();
            FilesRecentToolStrip.DropDownItems.Clear();
        }

        private void ModePlaceToggleGridSnap_Click(object sender, EventArgs e)
        {
            PlaceMode.SnapPlaceToGrid = !PlaceMode.SnapPlaceToGrid;
            ModePlaceToggleGridSnap.Checked = !ModePlaceToggleGridSnap.Checked;
        }

        private void MenuRedo_Click(object sender, EventArgs e)
        {
            UndoManager.Redo();
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            //Focus();
            Activate();
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
            this.Focus();
        }

        private void getLatestUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://github.com/DontBelieveMe/F.A.R.T/releases");

        }

        private void MenuPreferences_Click(object sender, EventArgs e)
        {
            new Forms.EditorPreferences().Show(this);
        }

    }
}
