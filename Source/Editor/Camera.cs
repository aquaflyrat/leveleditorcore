﻿using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace PolyKnights.LevelEditor
{
    public class Camera
    {
        private static readonly Cursor DragCursor = new Cursor(new MemoryStream(Properties.Resources.grab_cursor));

        public int x = 0, y = 0;
        private Point start = new Point();
        private bool dragging = false;
        
        public void MouseDown(Mouse mouse)
        {
            dragging = true;
            start = new Point(mouse.X, mouse.Y);
            Program.Editor.Cursor = DragCursor;
        }

        public void Reset()
        {
            x = 0;
            y = 0;
        }

        public void Move(Mouse mouse, Components.GridComponent grid, Canvas canvas)
        {
            if(dragging)
            {
                int deltaX = mouse.X - start.X;
                int deltaY = mouse.Y - start.Y;

                canvas.Move(deltaX, deltaY);

                foreach(var image in Level.Sprites)
                {
                    Point point = new Point();
                    point.X = image.Location.X + deltaX;
                    point.Y = image.Location.Y + deltaY;
                    image.Location = point;
                }
                
                x += (int)deltaX;
                y += (int) deltaY;

                EditorArea.Grid.Move(deltaX, deltaY);
                
                start.X = mouse.X;
                start.Y = mouse.Y;
            }
        }

        public void ButtonReleased()
        {
            Program.Editor.Cursor = Cursors.Arrow;
            start = new Point(0, 0);
            dragging = false;
        }
    }
}
