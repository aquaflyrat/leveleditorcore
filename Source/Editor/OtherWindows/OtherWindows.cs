﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor.OtherWindows
{
    public class OtherWindows
    {
        private static Dictionary<string, Dock> windows = new Dictionary<string, Dock>();
        public static bool ParentClosing = false;

        public void Add(string name, Dock instance)
        {
            if (new Settings.Settings().GetGroup("Editor Settings").GetValue<bool>("OpenTopOnLoad"))
            {
                instance.Show();
                instance.ToolStripItem.Checked = true;
            } else
            {
                instance.ToolStripItem.Checked = false;
            }
            windows.Add(name, instance);
        }

        public void Update()
        {
            foreach(var dock in windows.Values)
            {
                if (dock.Focused)
                {
                    dock.Tick();
                }
            }
        }
    }
}
