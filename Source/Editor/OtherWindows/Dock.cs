﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace PolyKnights.LevelEditor.OtherWindows
{
    public class Dock : System.Windows.Forms.Form
    {
        public ToolStripMenuItem ToolStripItem = null;

        public virtual void Tick() { }

        public void InitDock(ref ToolStripMenuItem item)
        {
            ToolStripItem = item;
            ToolStripItem.Click += new EventHandler(ToggleHidden);
            FormClosing += new FormClosingEventHandler(OnClosing);
            Owner = Program.Editor;
        }

        public void ToggleHidden(object sender, EventArgs e)
        {
            Visible = !Visible;
            ToolStripItem.Checked = !ToolStripItem.Checked;
        }

        protected void OnClosing(object sender, CancelEventArgs e)
        {
            if (OtherWindows.ParentClosing)
                return;

            e.Cancel = true;
            ToolStripItem.Checked = false;
            Visible = false;
        }
    }
}
