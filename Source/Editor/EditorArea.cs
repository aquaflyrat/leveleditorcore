﻿using System.Drawing;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor
{
    public class EditorArea : EditorComponentContainer
    {
        private Mouse mouse = new Mouse();
        private Image image;
        private Keys modifiers;
        
        private Font font = new Font("Courier New", 12, FontStyle.Regular);
        private Camera camera;

        public static Components.GridComponent Grid;
        private Canvas canvas;

        private const int width = 131072 / 64;
        private const int height = 131072 / 64;

        public EditorArea(Image bitmap, int w=-1, int h=-1)
        {
            this.image = bitmap;
            Level.ImageWidth = bitmap.Width;
            Level.ImageHeight = bitmap.Height;

            if(w == -1 && h == -1)
                Grid = new Components.GridComponent(width, height);
            else
                Grid = new Components.GridComponent(w, h);

            Mode.ModeSelector selector = new Mode.ModeSelector();
            selector.placeMode.canvas = canvas;
            selector.selectMode.canvas = canvas;
            selector.wireframeMode.canvas = canvas;

            components.Add(Grid);
            components.Add(selector);

            camera = new Camera();
            canvas = new Canvas();


        }
        
        public void Save(string filename)
        {
            Toolbox.FileSaver fileSaver = new Toolbox.FileSaver();
            fileSaver.SaveFile(filename, canvas.GetPolygons(), camera);
        }

        public void Load(string filename)
        {
            Toolbox.FileLoader fileLoader = new Toolbox.FileLoader();
            var listRef = canvas.GetPolygons();
            fileLoader.LoadFile(ref listRef, filename, canvas);
            Transform.TranslationTransform translate = new Transform.TranslationTransform();
            translate.MoveTriangles(canvas.GetPolygons(), camera.x, camera.y);
        }

        public void MouseWheel(MouseEventArgs e)
        {
        }

        public void Update(int mX, int mY, Keys modifiers)
        {
            mouse.SetPosition(mX, mY);
            this.modifiers = modifiers;

            base.Update(mouse, modifiers);

            using (Graphics g = Graphics.FromImage(image))
            {
                g.Clear(Color.Black);
                
                if (EditMode.State != EditState.Wireframe)
                {
                    canvas.Draw(g);
                }
                base.Render(g);
                
                g.DrawString(EditMode.State.ToString(), font, Brushes.Yellow, 10, 10);
            }
        }

        public new void OnClick()
        {
            base.OnClick();
        }
        
        public void MiddleMouseUp()
        {
           
        }

        public void MiddleMouseClick()
        {
            camera.MouseDown(mouse);
        }

        public void MiddleMouseDown(MouseEventArgs e)
        {
            base.MiddleMouseButtonDown();
            camera.Move(mouse, Grid, canvas);
        }

        public void OnKeyUp(KeyEventArgs e)
        {
        }
        
        public new void ProcessKeys(Keys keyData)
        {
            if(keyData == Keys.B)
            {
                var tmp = (Bitmap) image.Clone();
                image.Dispose();
                image = new Bitmap(tmp, new Size(tmp.Width * 2, tmp.Height * 2));
            }
            base.ProcessKeys(keyData);
        }

        public void OnMouseDown()
        {
            base.LeftMouseButtonDown();
        }

        public void OnMouseUp()
        {
            base.LeftMouseButtonReleased();
            camera.ButtonReleased();
        }

        public void RefreshImage(Image newImage)
        {
            this.image = newImage;
            //Grid.SetSize(image.Width, image.Height);
        }

        public void EnableSelectMode()
        {
            EditMode.State = EditState.Select;
        }

        public void EnablePlaceMode()
        {
            EditMode.State = EditState.Place;
        }

        public void EnableWireframeMode()
        {
            EditMode.State = EditState.Wireframe;
        }

        public void SwitchGrid()
        {
        }

        public void SwitchRotations()
        {
        }

        public void Clear()
        {
        }

        public void NewFile()
        {
            canvas.Clear();
        }
    }
}
