﻿using System;
using System.Collections.Generic;
using System.Drawing;
using PolyKnights.LevelEditor.Toolbox.Extensions;

namespace PolyKnights.LevelEditor
{
    public class SelectionGroup
    {
        private List<GameObject> selected = new List<GameObject>();

        public void Select(GameObject obj)
        {
            selected.Add(obj);
        }

        public void Deselect(GameObject obj)
        {
            selected.Remove(obj);
        }

        public void Clear()
        {
            selected.Clear();
        }

        public void Draw(Graphics g)
        {
            foreach (var obj in selected)
            {
                if (obj.Type == ObjectType.Polygon)
                {
                    Polygon poly = (Polygon)obj;
                    g.DrawPolygon(new Pen(Brushes.Yellow, 4), poly.Points);
                    g.DrawCircle(Pens.Red, 2, poly.Points[0], 5);
                    g.DrawCircle(Pens.Green, 2, poly.Points[1], 5);
                    g.DrawCircle(Pens.Blue, 2, poly.Points[2], 5);
                }
            }
        }

        public void SelectAll(List<GameObject> objs)
        {
            selected.AddRange(objs);
        }
    }
}
