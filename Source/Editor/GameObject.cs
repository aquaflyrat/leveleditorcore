﻿using System;
using System.Drawing;

namespace PolyKnights.LevelEditor
{
    public enum ObjectType
    {
        Image, Polygon
    }

    public enum Angle
    {
        Zero = 0, Ninety = 1, OneHundredEighty = 2, ThreeSixty = 3
    }

    public abstract class GameObject
    {
        public ObjectType Type { get;  protected set; }
        public abstract void Draw(Graphics g);

        public virtual void Outline(Graphics g)
        {
        }
    }
    
    public class Polygon : GameObject
    {
        public PointF[] Points = { new PointF(0, 0), new PointF(0, 0), new PointF(0, 0) };
        public Brush Brush;
        public Angle Angle = Angle.Zero;
        public bool IsFlipped = false;
        
        public Polygon()
        {
            Type = ObjectType.Polygon;
        }

        public override void Draw(Graphics g)
        {
            g.FillPolygon(Brush, Points);
        }

        public Polygon Clone()
        {
            Polygon polygon = new Polygon();
            polygon.Angle = Angle;
            polygon.Points = (PointF[])Points.Clone();
            polygon.Brush = (Brush) Brush.Clone();
            polygon.IsFlipped = IsFlipped;
            return polygon;
        }

        public bool Inside(Polygon other)
        {
            bool col1 = Points[0].X == other.Points[0].X && Points[0].Y == other.Points[0].Y;
            bool col2 = Points[1].X == other.Points[1].X && Points[1].Y == other.Points[1].Y;
            bool col3 = Points[2].X == other.Points[2].X && Points[2].Y == other.Points[2].Y;
            return col1 && col2 && col3;
        }
    }

}
