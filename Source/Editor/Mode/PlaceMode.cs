﻿using System.Drawing;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor
{
    public enum WhatToPlace
    {
        Image, Polygon
    }
    
    public class PlaceMode : EditorComponent
    {
        public static Polygon indicator = new Polygon();
        private Mouse mouse;
        public static WhatToPlace Placing = WhatToPlace.Polygon;
        public static bool SnapPlaceToGrid = true;
        
        public Canvas canvas { get; set; }

        public override void Render(Graphics g)
        {
            switch (Placing)
            {
                case WhatToPlace.Image:
                    if (Level.ImagePlace != null)
                    {
                        float sx, sy;

                        if (SnapPlaceToGrid)
                        {
                            var settings = new Settings.Settings();
                            var group = settings.GetGroup("Map Properties");
                            int snapW = (int)group.GetValue("GridWidth");
                            int snapH = (int)group.GetValue("GridHeight");
                            sx = (float)(System.Math.Floor((double)mouse.X / snapW) * snapW);
                            sy = (float)(System.Math.Floor((double)mouse.Y / snapH) * snapH);
                            var grid = EditorArea.Grid.Position;
                            float gx = (float)(System.Math.Floor((double)grid.X / snapW) * snapW);
                            float gy = (float)(System.Math.Floor((double)grid.Y / snapH) * snapH);
                            sx += grid.X - gx;
                            sy += grid.Y - gy;
                        } else
                        {
                            sx = mouse.X;
                            sy = mouse.Y;
                        }

                        // Draw an opaque image.
                        Bitmap tmp = (Bitmap) Level.ImagePlace.Image.Clone();
                        for (int y = 0; y < tmp.Height; y++)
                        {
                            for (int x = 0; x < tmp.Width; x++)
                            {
                                tmp.SetPixel(x, y, Color.FromArgb(100, tmp.GetPixel(x, y)));
                            }
                        }

                        g.DrawImage(tmp, sx, sy);
               
                    }
                    break;
                case WhatToPlace.Polygon:

                    g.FillPolygon(indicator.Brush, indicator.Points);
                    Color c = Color.FromArgb(200, 64, 64, 64);
                    g.DrawPolygon(new Pen(new SolidBrush(c), 3), indicator.Points);
                    break;
            }
        }

        public override void Update(Mouse mouse, Keys modifiers)
        {
            this.mouse = mouse;

            Color indicatorColor = Color.FromArgb(155, Level.CurrentColor);
            indicator.Brush = new SolidBrush(indicatorColor);

            var settings = new Settings.Settings();
            var group = settings.GetGroup("Map Properties");
            int snapW = (int)group.GetValue("PlaceSnapX");
            int snapH = (int)group.GetValue("PlaceSnapY");

            float sx, sy;
            if (SnapPlaceToGrid)
            {
                var grid = EditorArea.Grid.Position;

                sx = (float)(System.Math.Floor((double)mouse.X / snapW) * snapW);
                sy = (float)(System.Math.Floor((double)mouse.Y / snapH) * snapH);

                float gx = (float)(System.Math.Floor((double)grid.X / snapW) * snapW);
                float gy = (float)(System.Math.Floor((double)grid.Y / snapH) * snapH);
                sx += grid.X - gx;
                sy += grid.Y - gy;
            }
            else
            {
                sx = mouse.X;
                sy = mouse.Y;
            }
            
            var polyGroup = settings.GetGroup("Polygon Properties");
            int _base = (int)polyGroup.GetValue("Base");
            int _height = (int)polyGroup.GetValue("Height");
            sx += 64;
            sy += snapH;
            indicator.Points[0] = new PointF(sx - _base, sy);
            indicator.Points[1] = new PointF(sx, sy);
            indicator.Points[2] = new PointF(sx, sy - _height);

            if (indicator.IsFlipped)
            {
                Transform.PolyFlipInPlace flip = new Transform.PolyFlipInPlace();
                flip.Flip(ref indicator);
            }
        }

        public override void OnClick()
        {
            LevelEditor.Unsave();
            switch (Placing)
            {
                case WhatToPlace.Image:
                    if (Level.ImagePlace != null)
                    {
                        Sprite sprite = new Sprite(Level.ImagePlace);
                        var settings = new Settings.Settings();
                        var group = settings.GetGroup("Map Properties");
                        int snapW = (int)group.GetValue("GridWidth");
                        int snapH = (int)group.GetValue("GridHeight"); ;
                        int sx = (int)(System.Math.Floor((double)mouse.X / snapW) * snapW);
                        int sy = (int)(System.Math.Floor((double)mouse.Y / snapH) * snapH);
                        sprite.Location = new Point(sx, sy);
                        Level.Sprites.Add(sprite);
                    }
                    break;
                case WhatToPlace.Polygon:

                    // Check for trying to place on another polygon
                    foreach(Polygon poly in canvas.GetPolygons())
                    {
                        if (indicator.Inside(poly))
                            return;
                    }

                    LevelEditor.UndoManager.ExecuteCommand(new Commands.PlaceCommand(indicator, canvas));
                    break;
            }
        
        }

        public override void ProcessKeys(Keys keyData)
        {
            if(keyData == Keys.F)
            {
                indicator.IsFlipped = !indicator.IsFlipped;
            } if(keyData == Keys.L)
            {
                LevelEditor.UserError(canvas.GetPolygons().Count);
            } if (keyData == Keys.P)
            {
                if (Placing == WhatToPlace.Polygon)
                    Placing = WhatToPlace.Image;
                else
                    Placing = WhatToPlace.Polygon;
            }
        }

    }
}

