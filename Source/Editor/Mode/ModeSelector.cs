﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor.Mode
{
    public class ModeSelector : EditorComponent
    {
        public SelectMode selectMode = new SelectMode();
        public PlaceMode placeMode = new PlaceMode();
        public WireframeMode wireframeMode = new WireframeMode();

        public override void OnClick()
        {
            switch (EditMode.State)
            {
                case EditState.Place:
                    placeMode.OnClick();
                    break;
                case EditState.Select:
                    selectMode.OnClick();
                    break;
                case EditState.Wireframe:
                    wireframeMode.OnClick();
                    break;
                default: break;
            }
        }

        public override void ProcessKeys(Keys keyData)
        {
            switch (EditMode.State)
            {
                case EditState.Place:
                    placeMode.ProcessKeys(keyData);
                    break;
                case EditState.Select:
                    selectMode.ProcessKeys(keyData);
                    break;
                case EditState.Wireframe:
                    wireframeMode.ProcessKeys(keyData);
                    break;
                default: break;
            }
        }

        public override void Render(Graphics g)
        {
            switch (EditMode.State)
            {
                case EditState.Place:
                    placeMode.Render(g);
                    break;
                case EditState.Select:
                    selectMode.Render(g);
                    break;
                case EditState.Wireframe:
                    wireframeMode.Render(g);
                    break;
                default: break;
            }
        }

        public override void Update(Mouse mouse, Keys modifiers)
        {
            switch (EditMode.State)
            {
                case EditState.Place:
                    placeMode.Update(mouse, modifiers);
                    break;
                case EditState.Select:
                    selectMode.Update(mouse, modifiers);
                    break;
                case EditState.Wireframe:
                    wireframeMode.Update(mouse, modifiers);
                    break;
                default: break;
            }
        }

        public override void LeftMouseButtonDown()
        {
            switch (EditMode.State)
            {
                case EditState.Place:
                    placeMode.LeftMouseButtonDown();
                    break;
                case EditState.Select:
                    selectMode.LeftMouseButtonDown();
                    break;
                case EditState.Wireframe:
                    wireframeMode.LeftMouseButtonDown();
                    break;
                default: break;
            }
        }

        public override void LeftMouseButtonReleased()
        {
            switch (EditMode.State)
            {
                case EditState.Place:
                    placeMode.LeftMouseButtonReleased();
                    break;
                case EditState.Select:
                    selectMode.LeftMouseButtonReleased();
                    break;
                case EditState.Wireframe:
                    wireframeMode.LeftMouseButtonReleased();
                    break;
                default: break;
            }
        }
    }
}
