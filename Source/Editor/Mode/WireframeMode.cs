﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor
{
    public class WireframeMode : EditorComponent
    {
        public Canvas canvas;

        public override void Render(Graphics g)
        {
            foreach (var poly in canvas.GetPolygons())
            {
                g.DrawPolygon(Pens.White, poly.Points);
            }
        }
    }
}

