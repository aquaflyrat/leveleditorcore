﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using PolyKnights.LevelEditor.Toolbox.Extensions;

namespace PolyKnights.LevelEditor
{
    public class SelectMode : EditorComponentContainer
    {
        private List<Polygon> selected = new List<Polygon>();
        private Components.SelectionRectangleComponent selector;

        private Mouse mouse;
        private Keys modifiers;

        private Transform.PolyPointModifier pointModifier = new Transform.PolyPointModifier();
        private Commands.PointModifyPolyCommand pointPolyModify;
        private Transform.PolyMouseTransform mouseTransform = new Transform.PolyMouseTransform();
        private Polygon indicator = new Polygon();

        private SelectionGroup selectedGroup;
        public Canvas canvas { get; set; }

        public SelectMode()
        {
            selector = new Components.SelectionRectangleComponent();

            selectedGroup = new SelectionGroup();

            components.Add(selector);
        }

        public override void Render(Graphics g)
        {
            base.Render(g);
            selectedGroup.Draw(g);
        }

        public override void Update(Mouse mouse, Keys modifiers)
        {
            base.Update(mouse, modifiers);
            this.mouse = mouse;
            this.modifiers = modifiers;
            pointModifier.Update(selected, mouse, modifiers, Level.ImageHeight);
            mouseTransform.Update(selected, mouse);
            
            var settings = new Settings.Settings();
            var group = settings.GetGroup("Map Properties");
            int snapW = (int)group.GetValue("PlaceSnapX");
            int snapH = (int)group.GetValue("PlaceSnapY");

            float sx, sy;
            if (PlaceMode.SnapPlaceToGrid)
            {
                sx = (float)(System.Math.Floor((double)mouse.X / snapW) * snapW);
                sy = (float)(System.Math.Floor((double)mouse.Y / snapH) * snapH);
            }
            else
            {
                sx = mouse.X;
                sy = mouse.Y;
            }

            var polyGroup = settings.GetGroup("Polygon Properties");
            int _base = (int)polyGroup.GetValue("Base");
            int _height = (int)polyGroup.GetValue("Height");

            indicator.Points[0] = new PointF(sx, sy + _height);
            indicator.Points[1] = new PointF(sx + _base, sy + _height);
            indicator.Points[2] = new PointF(sx + _base, sy);

            if (indicator.IsFlipped)
            {
                Transform.PolyFlipInPlace flip = new Transform.PolyFlipInPlace();
                flip.Flip(ref indicator);
            }
        }
        
        public override void OnClick()
        {
            Polygon poly = Toolbox.Editing.GetPolygonAtMouse(canvas.GetPolygons(), mouse);
            
            if (poly == null)
            {
                if (pointModifier.CurrentlyModifying())
                {
                    return;
                }
                selectedGroup.Clear();

                return;
            }

            pointPolyModify = new Commands.PointModifyPolyCommand(poly, pointModifier);

            if (!modifiers.HasFlag(Keys.Control))
                selectedGroup.Clear();


            selectedGroup.Select(poly);
            base.OnClick();
        }

        public override void ProcessKeys(Keys keyData)
        {
            Transform.TranslationTransform translate = new Transform.TranslationTransform();
            if(selected.Count > 0)
                translate.MoveOnKeys(selected[0], keyData, 64);

            if (keyData == (Keys.Control | Keys.A))
            {
                selectedGroup.SelectAll(canvas.GetAllObjects());
            }
            else if (keyData == (Keys.R))
            {
                LevelEditor.Unsave();
                LevelEditor.UndoManager.ExecuteCommand(new Commands.RotateCommand((Polygon) selected[0]));
            }
            else if (keyData == (Keys.Delete))
            {
                if (selected.Count < 1)
                    return;

                LevelEditor.Unsave();
                LevelEditor.UndoManager.ExecuteCommand(new Commands.DeleteCommand(selected[0], canvas));
                
                selected.Clear();
            } else if(keyData == (Keys.F))
            {
                for(int i = 0; i < selected.Count; i++)
                {
                    LevelEditor.UndoManager.ExecuteCommand(new Commands.FlipCommand((Polygon) selected[i]));
                }
            } else if(keyData == (Keys.D | Keys.Control))
            {
                //var size = selected.Count;
                //for(int i = 0; i < size; i++)
                //{
                //    var newPoly = selected[i].Clone();
                //    selected.Add(newPoly);
                //    selected.Remove(selected[i]);
                //    Level.Triangles.Add(newPoly);
                //}
                //LevelEditor.UndoManager.ExecuteCommand(new Commands.DuplicatePolyCommand((Polygon) selected[0], (selected));
            }

            if (keyData == Keys.L)
            {
                LevelEditor.UserError(canvas.GetPolygons().Count);
            }

            if (keyData == Keys.Escape)
                selected.Clear();

            base.ProcessKeys(keyData);
        }

        public override void LeftMouseButtonDown()
        {
            base.LeftMouseButtonDown();

            if (!mouseTransform.Moving())
            {
                selector.SelectNessersaryTriangles(ref selected, mouseTransform.Moving(), canvas);
                pointModifier.OnMouseDown(selected, mouse, 5);
            }

            if(!pointModifier.CurrentlyModifying())
                mouseTransform.OnMouseDown(selected, indicator, mouse);
        }

        public override void LeftMouseButtonReleased()
        {
            if(pointModifier.CurrentlyModifying())
                LevelEditor.UndoManager.ExecuteCommand(pointPolyModify);
            pointModifier.OnMouseUp();
            mouseTransform.OnMouseUp();

            base.LeftMouseButtonReleased();
        }

        public override void MiddleMouseButtonDown()
        {
            base.MiddleMouseButtonDown();
        }
    }
}
