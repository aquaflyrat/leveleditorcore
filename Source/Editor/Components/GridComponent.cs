﻿using System.Drawing;

namespace PolyKnights.LevelEditor.Components
{
    public class GridComponent : EditorComponent
    {
        public static bool GridEnabled = true;
        public Point Position = new Point(0, 0);

        public int Width, Height;

        public GridComponent(int w, int h)
        {
            this.Width = w * 64;
            this.Height = (h * 64);
        }

        public void SetSize(int w, int h)
        {
            this.Width = w;
            this.Height = h;
        }

        public override void Render(Graphics g)
        {
            if (!GridEnabled)
                return;

            var group = new Settings.Settings().GetGroup("Map Properties");
            Color tmp = (Color)group.GetValue("GridColor");
            Pen pen = new Pen(Color.FromArgb((int)group.GetValue("GridColorAlpha"), tmp.R, tmp.G, tmp.B));

            for (int y = 0; y < Height; y += (int)group.GetValue("GridHeight"))
            {
                g.DrawLine(pen, new Point(Position.X + 0, Position.Y + y), new Point(Position.X + Width, Position.Y + y));
            }

            for (int x = 0; x < Width; x += (int)group.GetValue("GridWidth"))
            {
                g.DrawLine(pen, new Point(Position.X +  x, Position.Y + 0), new Point(Position.X + x, Position.Y + Height));
            }
        }
        
        public void IncreaseWidth()
        {
        }

        public void Move(int xOffset, int yOffset)
        {
            Position = new Point(Position.X + xOffset, Position.Y + yOffset);
        }

        public void SetPosition(int x, int y)
        {
            Position = new Point(x, y);
        }
    }
}
