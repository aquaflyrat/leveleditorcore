﻿using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PolyKnights.LevelEditor.Components
{
    public class SelectionRectangleComponent : EditorComponent
    {
        public Rectangle SelectionRect = new Rectangle();
        private bool selectingPolys = false;
        private Point selectionClickPos = new Point();
        private Mouse mouse;
        private bool cannotDrawRect = false;

        public override void Render(Graphics g)
        {
            if (cannotDrawRect == false)
                return;

            g.FillRectangle(new SolidBrush(Color.FromArgb(155, 58, 135, 171)), SelectionRect);
            g.DrawRectangle(Pens.LightBlue, SelectionRect);
        }

        public void SelectNessersaryTriangles(ref List<Polygon> triangles, bool modifiying, Canvas canvas)
        {
            if (!selectingPolys)
                return;

            cannotDrawRect = modifiying;
            if (modifiying)
                return;
            
            foreach (var poly in canvas.GetPolygons())
            {
                if (Toolbox.Maths.PolygonInsideRect(poly, SelectionRect))
                {
                    if (triangles.Contains(poly))
                        continue;

                    triangles.Add(poly);
                }
            }   
        }

        public override void LeftMouseButtonDown()
        {
            if (cannotDrawRect)
                return;

            if (!selectingPolys)
            {
                selectingPolys = true;
                selectionClickPos.X = mouse.X;
                selectionClickPos.Y = mouse.Y;
            }

            if (cannotDrawRect)
                return;

            if (selectingPolys)
            {
                SelectionRect = new Rectangle(
                     System.Math.Min(selectionClickPos.X, mouse.X),
                     System.Math.Min(selectionClickPos.Y, mouse.Y),
                     System.Math.Abs(mouse.X - selectionClickPos.X),
                     System.Math.Abs(mouse.Y - selectionClickPos.Y));
            }
        }

        public override void LeftMouseButtonReleased()
        {
            SelectionRect = new Rectangle();
            selectingPolys = false;
        }

        public override void Update(Mouse mouse, Keys modifiers)
        {
            this.mouse = mouse;
        }
    }
}
