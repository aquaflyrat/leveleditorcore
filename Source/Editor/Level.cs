﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor
{
    public class Level
    {
        public static int ImageWidth = -1;
        public static int ImageHeight = -1;
        public static Color CurrentColor = Color.LightGray;
        public static Sprite ImagePlace = null;
        public static List<Sprite> Sprites = new List<Sprite>();
    }
}
