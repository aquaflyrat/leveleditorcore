﻿using System.Drawing;
using System;


namespace PolyKnights.LevelEditor
{
    public class Sprite : GameObject, IDisposable
    {
        public Image Image { get; private set; }
        public Point Location { get; set; }
        public Rectangle Bounds { get; private set; }
        public Sprites.SpriteInfo Info { get; set; }

        public Sprite(Image image, Rectangle bounds, Sprites.SpriteInfo info)
        {
            this.Image = image;
            this.Bounds = bounds;
            this.Location = bounds.Location;
            this.Info = info;
        }

        public Sprite(Image image, Rectangle bounds)
        {
            this.Image = image;
            this.Bounds = bounds;
            this.Location = bounds.Location;
            this.Info = null;
        }
        
        public Sprite(Sprite sprite): this(sprite.Image, sprite.Bounds, sprite.Info)
        {

        }

        public bool Contains(Point point)
        {
            return Bounds.Contains(point);
        }

        public void Dispose()
        {
            Image.Dispose();
        }

        public Sprite Clone()
        {
            return new Sprite(Image, Bounds);
        }

        public override void Draw(Graphics g)
        {
            g.DrawImage(Image, Location);
        }
    }
}
