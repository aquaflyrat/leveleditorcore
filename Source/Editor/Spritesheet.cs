﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PolyKnights.LevelEditor
{
    public class Spritesheet
    {
        public Image Sheet { get; private set; }
        private string path = "NO PATH";
        private List<Sprite> sprites = new List<Sprite>();

        public Spritesheet(Image image)
        {
            Sheet = image;
        }

        public Spritesheet(string fileName)
        {
            Sheet = Image.FromFile(fileName);
            path = fileName;
        }

        public Sprite GetSprite(Point point)
        {
            foreach(var sp in sprites)
            {
                if (sp.Contains(point))
                    return sp;
            }

            BitmapUtil.SpriteExtractor extracter = new BitmapUtil.SpriteExtractor();
            Sprite sprite = extracter.ExtractSprite(Sheet, point, Color.FromArgb(0, 225, 255, 255));
            sprites.Add(sprite);
            return sprite;
        }

        public Sprite GetSprite(Rectangle bounds)
        {
            Bitmap bitmap = Sheet as Bitmap;
            return new Sprite(bitmap.Clone(bounds, Sheet.PixelFormat), bounds);
        }
    }
}
