﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyKnights.LevelEditor.Commands
{
    public class DuplicateCommand : ICommand
    {
        private Polygon newPoly;
        private Polygon oldPoly;
        private List<Polygon> selected;
        private Canvas canvas;

        public DuplicateCommand(Polygon template, List<Polygon> selected, Canvas canvas)
        {
            newPoly = template.Clone();
            oldPoly = template;
            this.selected = selected;
            this.canvas = canvas;
        }

        public void Execute()
        {
            selected.Remove(oldPoly);
            canvas.AddObject(newPoly);
            selected.Add(newPoly);
        }

        public void Redo()
        {
            selected.Remove(oldPoly);
            canvas.AddObject(newPoly);
            selected.Add(newPoly);
        }

        public void Undo()
        {
            canvas.RemoveObject(newPoly);
            if (selected.Contains(newPoly))
                selected.Remove(newPoly);
        }
    }
}
