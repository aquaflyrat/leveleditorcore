﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyKnights.LevelEditor.Commands
{
    public class FlipCommand : ICommand
    {
        Polygon triangle;

        public FlipCommand(Polygon triangle)
        {
            this.triangle = triangle;
        }

        public void Execute()
        {
            Transform.PolyFlipInPlace flip = new Transform.PolyFlipInPlace();
            triangle.IsFlipped = !triangle.IsFlipped;
            flip.Flip(ref triangle);
        }

        public void Redo()
        {
            Transform.PolyFlipInPlace flip = new Transform.PolyFlipInPlace();
            triangle.IsFlipped = !triangle.IsFlipped;
            flip.Flip(ref triangle);
        }

        public void Undo()
        {
            Transform.PolyFlipInPlace flip = new Transform.PolyFlipInPlace();
            triangle.IsFlipped = !triangle.IsFlipped;
            flip.Flip(ref triangle);
        }
    }
}
