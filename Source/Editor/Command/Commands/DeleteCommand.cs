﻿
namespace PolyKnights.LevelEditor.Commands
{
    public class DeleteCommand : ICommand
    {
        private Polygon triangle;
        private Canvas canvas;

        public DeleteCommand(Polygon triangle, Canvas canvas)
        {
            this.triangle = triangle;
            this.canvas = canvas;
        }

        public void Execute()
        {
            canvas.RemoveObject(triangle);
        }

        public void Redo()
        {
            canvas.RemoveObject(triangle);
        }

        public void Undo()
        {
            canvas.AddObject(triangle);
        }
    }
}
