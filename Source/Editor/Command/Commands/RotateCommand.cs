﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyKnights.LevelEditor.Commands
{
    public class RotateCommand : ICommand
    {
        private Polygon triangle;

        public RotateCommand(Polygon triangle)
        {
            this.triangle = triangle;
        }

        public void Execute()
        {
            Transform.PolyRotateTransform rotate = new Transform.PolyRotateTransform();
            rotate.Rotate(ref triangle);
        }

        public void Undo()
        {
            Transform.PolyRotateTransform rotate = new Transform.PolyRotateTransform();
            Angle prev = rotate.GetPreviousAngle(triangle);
            while (triangle.Angle != prev)
                rotate.Rotate(ref triangle);
        }

        public void Redo()
        {
            Transform.PolyRotateTransform rotate = new Transform.PolyRotateTransform();
            rotate.Rotate(ref triangle);
        }
    }
}
