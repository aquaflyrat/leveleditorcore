﻿using System;
using System.Drawing;

namespace PolyKnights.LevelEditor.Commands
{
    public class PlaceCommand : ICommand
    {
        private Polygon placedPoly;
        private Canvas canvas;

        public PlaceCommand(Polygon template, Canvas canvas)
        {
            placedPoly = template.Clone();
            Color color = new Pen(template.Brush).Color;
            color = Color.FromArgb(255, color.R, color.G, color.B);
            placedPoly.Brush = new SolidBrush(color);
            this.canvas = canvas;
        }

        public void Execute()
        {
            canvas.AddPolygon(placedPoly);
        }

        public void Undo()
        {
            canvas.RemovePolygon(placedPoly);
        }

        public void Redo()
        {
            canvas.AddPolygon(placedPoly);
        }
    }
}
