﻿using System.Drawing;

namespace PolyKnights.LevelEditor.Commands
{
    public class PointModifyPolyCommand : ICommand
    {
        private Polygon oldTriangle;
        private Polygon newTriangle;
        private Transform.PolyPointModifier modifier;

        public PointModifyPolyCommand(Polygon triangle, Transform.PolyPointModifier mod)
        {
            oldTriangle = triangle.Clone();
            newTriangle = triangle;
            modifier = mod;
        }

        public void Execute()
        {
        }

        public void Redo()
        {
            SwitchTriangles();
        }

        public void Undo()
        {
            SwitchTriangles();
        }

        private void SwitchTriangles()
        {
            Polygon tmp = newTriangle.Clone();
            newTriangle.Points[0] = oldTriangle.Points[0];
            newTriangle.Points[1] = oldTriangle.Points[1];
            newTriangle.Points[2] = oldTriangle.Points[2];
            oldTriangle = tmp;
        }
    }
}
