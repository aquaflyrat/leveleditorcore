﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyKnights.LevelEditor.Commands
{
    public class MoveCommand : ICommand
    {
        private Polygon triangle;
        private int moveX, moveY;

        public MoveCommand(Polygon triangle, int moveX, int moveY)
        {
            this.triangle = triangle;
            this.moveX = moveX;
            this.moveY = moveY;
        }

        public void Execute()
        {
            Transform.TranslationTransform translate = new Transform.TranslationTransform();
            translate.MoveTriangle(ref triangle, moveX, moveY);
        }

        public void Redo()
        {
            Transform.TranslationTransform translate = new Transform.TranslationTransform();
            translate.MoveTriangle(ref triangle, moveX, moveY);
        }

        public void Undo()
        {
            Transform.TranslationTransform translate = new Transform.TranslationTransform();
            translate.MoveTriangle(ref triangle, -moveX, -moveY);
        }
    }
}
