﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyKnights.LevelEditor
{
    public interface ICommand
    {
        void Execute();
        void Undo();
        void Redo();
    }

    public class CommandManager
    {
        private Stack<ICommand> undoStack = new Stack<ICommand>();
        private Stack<ICommand> redoStack = new Stack<ICommand>();

        public void ExecuteCommand(ICommand cmd)
        {
            cmd.Execute();
            redoStack.Clear();
            undoStack.Push(cmd);
        }

        public void Undo()
        {
            if(undoStack.Count > 0)
            {
                ICommand cmd = undoStack.Pop();
                cmd.Undo();
                redoStack.Push(cmd);
                
            }
        }

        public void Redo()
        {
            if(redoStack.Count > 0)
            {
                ICommand cmd = redoStack.Pop();
                cmd.Redo();
                undoStack.Push(cmd);
            }
        }
    }
}
