﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PolyKnights.LevelEditor.BitmapUtil
{
    public class BitmapCreator
    {
        public Image CreateImage(string path, int mapWidth, int mapHeight, int tileWidth, int tileHeight)
        {
            Bitmap bitmap = new Bitmap(tileWidth * mapWidth, tileHeight * mapHeight);

            for(int x = 0; x < tileWidth * mapWidth; x++)
            {
                for(int y = 0; y < tileHeight * mapHeight; y++)
                {
                    bitmap.SetPixel(x, y, Color.FromArgb(0, 255, 255, 255));
                }
            }

            for(int x = 0; x < mapWidth * tileWidth; x += tileWidth)
            {
                for(int y = 0; y < mapHeight * tileHeight; y++)
                {
                    bitmap.SetPixel(x, y, Color.Black);
                }
            }

            for (int y = 0; y < mapHeight * tileHeight; y += tileHeight)
            {
                for (int x = 0; x < mapWidth * tileWidth; x++)
                {
                    bitmap.SetPixel(x, y, Color.Black);
                }
            }

            bitmap.Save(path);
            return null;
        }
    }
}
