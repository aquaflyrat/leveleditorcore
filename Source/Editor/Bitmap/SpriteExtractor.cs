﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PolyKnights.LevelEditor.BitmapUtil
{
    public class SpriteExtractor
    {
        private Point GetBound(Bitmap image, Color clear, Point origin, int jump, int lewayX, int lewayY)
        {
            int x = origin.X;
            int y = origin.Y;
            Color pixel = image.GetPixel(origin.X, origin.Y);
            
            while(pixel.A != 0)
            {
                x -= jump;
                pixel = image.GetPixel(x - lewayX, origin.Y);
            }

            pixel = image.GetPixel(origin.X, origin.Y);
            while (pixel.A != 0)
            {
                y -= jump;
                pixel = image.GetPixel(origin.X, y - lewayY);
            }
            return new Point(x, y);
        }

        public Sprite ExtractSprite(Image original, Point position, Color clearColor)
        {
            Bitmap image = (Bitmap)original;
            Rectangle rect = new Rectangle();

            rect.Location = GetBound(image, clearColor, position, 1, 1, 1);

            Point size = GetBound(image, clearColor, position, -1, 0, 0);
            rect.Width = size.X - rect.X - 1;
            rect.Height = size.Y - rect.Y - 1;
            
            if (rect.X < 0 || rect.Y < 0 || rect.Width < 0 || rect.Height < 0)
                return null;

            Bitmap result = image.Clone(rect, original.PixelFormat);
            result.MakeTransparent(clearColor);
            return new Sprite(result, rect);
        }
    }
}
