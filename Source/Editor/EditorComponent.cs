﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor
{
    public class EditorComponent
    {
        public virtual void Update(Mouse mouse, Keys modifiers) { }
        public virtual void Render(Graphics g) { }

        public virtual void ProcessKeys(Keys keyData) { }

        public virtual void OnClick() { }
        public virtual void MouseMove() { }
        public virtual void MouseReleased() { }

        public virtual void LeftMouseButtonDown() { }
        public virtual void LeftMouseButtonReleased() { }
        public virtual void MiddleMouseButtonDown() { }
    }
}
