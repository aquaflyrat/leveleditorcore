﻿using System.Windows.Forms;
using System.Collections.Generic;
using System;

namespace PolyKnights.LevelEditor
{
    public class KeyboardShortcuts
    {
        private Dictionary<Keys, Action> shortcuts = new Dictionary<Keys, Action>();

        public void AddShortcut(Keys shortcut, Action callback, string text, string desc, ref ToolStripMenuItem item)
        {
            AddShortcut(shortcut, callback, text, desc);
            item.ShortcutKeyDisplayString = text;
            item.ShowShortcutKeys = true;
        }

        public void AddShortcut(Keys shortcut, Action callback, string text, string desc)
        {
            Keys s = shortcut;
            shortcuts.Add(shortcut, callback);
        }

        public void AddShortcut(string prequsite, string text, string desc)
        {
        }
        
        public void Process(Keys keyData)
        {
            foreach(var pair in shortcuts)
            {
                if(pair.Key == keyData)
                {
                    pair.Value.DynamicInvoke();
                }
            }
        }
    }
}
