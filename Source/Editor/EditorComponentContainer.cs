﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor
{
    public class EditorComponentContainer : EditorComponent
    {
        protected List<EditorComponent> components = new List<EditorComponent>();

        public override void LeftMouseButtonDown()
        {
            foreach(var component in components)
            {
                component.LeftMouseButtonDown();
            }
        }

        public override void LeftMouseButtonReleased()
        {
            foreach (var component in components)
            {
                component.LeftMouseButtonReleased();
            }
        }

        public override void MiddleMouseButtonDown()
        {
            foreach (var component in components)
            {
                component.MiddleMouseButtonDown();
            }
        }

        public override void MouseMove()
        {
            foreach (var component in components)
            {
                component.MouseMove();
            }
        }

        public override void MouseReleased()
        {
            foreach (var component in components)
            {
                component.MouseReleased();
            }
        }

        public override void OnClick()
        {
            foreach (var component in components)
            {
                component.OnClick();
            }
        }

        public override void ProcessKeys(Keys keyData)
        {
            foreach (var component in components)
            {
                component.ProcessKeys(keyData);
            }
        }

        public override void Render(Graphics g)
        {
            foreach (var component in components)
            {
                component.Render(g);
            }
        }

        public override void Update(Mouse mouse, Keys modifiers)
        {
            foreach (var component in components)
            {
                component.Update(mouse, modifiers);
            }
        }
    }
}
