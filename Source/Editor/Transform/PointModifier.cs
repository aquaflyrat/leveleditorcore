﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace PolyKnights.LevelEditor.Transform
{
    public class PolyPointModifier
    {
        private Polygon triangle = null;

        private bool modifying = false;
        private int index = -1;

        public void Update(List<Polygon> selected, Mouse mouse, Keys modifiers, int height)
        {
            
            Settings.Settings settings = new Settings.Settings();
            var group = settings.GetGroup("Map Properties");
            if (selected.Count != 1)
                return;

            if (selected[0].Type != ObjectType.Polygon)
                return;

            Polygon obj = (Polygon)selected[0];

            Point newPoint = new Point(0, 0);
            if(modifiers.HasFlag(Keys.Shift))
            {
                newPoint.X = mouse.X - mouse.X % (int) group.GetValue("PointSnapX");
                newPoint.Y = mouse.Y + (height - mouse.Y) % (int)group.GetValue("PointSnapY");
            } else
            {
                newPoint = mouse.Point();
            }

            var grid = EditorArea.Grid.Position;

            if (index < 0)
                return;

            modifying = true;
            obj.Points[index] = newPoint;
        }

        public bool CurrentlyModifying()
        {
            return modifying;
        }

        public void OnMouseDown(List<Polygon> selected, Mouse mouse, int radius)
        {
            if (selected.Count != 1)
                return;

            if (selected[0].Type != ObjectType.Polygon)
                return;

            Polygon obj = (Polygon)selected[0];

            for(int i = 0; i < 3; i++)
            {
                var point = obj.Points[i];
                if(Toolbox.Maths.PointInsideCircle(mouse.Point(), point, radius) && i != index)
                {
                    modifying = true;
                    this.triangle = obj;
                    index = i;
                }
            }
        }

        public void OnMouseUp()
        {
            modifying = false;
            index = -1;
        }
    }
}
