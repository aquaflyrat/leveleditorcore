﻿using System.Drawing;
using System.Collections.Generic;

namespace PolyKnights.LevelEditor.Transform
{
    public class PolyMouseTransform
    {
        private bool modifying = false;

        public PolyMouseTransform()
        {
        }
        public void Update(List<Polygon> selected, Mouse mouse)
        {
        }

        public void OnMouseDown(List<Polygon> selected, Polygon template, Mouse mouse)
        {
        }

        public void OnMouseUp()
        {
        }

        public bool Moving()
        {
            return modifying;
        }
    }
}
