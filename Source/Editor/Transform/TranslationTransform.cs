﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor.Transform
{
    public class TranslationTransform
    {
        public void MoveTriangle(ref Polygon triangle, float xOffset, float yOffset)
        {
            triangle.Points[0].X += xOffset;
            triangle.Points[0].Y += yOffset;
            triangle.Points[1].X += xOffset;
            triangle.Points[1].Y += yOffset;
            triangle.Points[2].X += xOffset;
            triangle.Points[2].Y += yOffset;
        }

        public void MoveTriangles(List<Polygon> triangles, int x, int y)
        {
            for(int i = 0; i < triangles.Count; i++)
            {
                var poly = triangles[i];
                MoveTriangle(ref poly, x, y);
            }
        }

        public void MoveOnKeys(GameObject obj, Keys keys, int moveAmount)
        {
            if (obj.Type != ObjectType.Polygon)
                return;

            Polygon triangle = (Polygon)obj;

            if(keys == Keys.Up)
            {
                LevelEditor.UndoManager.ExecuteCommand(new Commands.MoveCommand(triangle, 0, -moveAmount));
            } else if(keys == Keys.Down)
            {
                LevelEditor.UndoManager.ExecuteCommand(new Commands.MoveCommand(triangle, 0, moveAmount));
            }
            else if(keys == Keys.Left)
            {
                LevelEditor.UndoManager.ExecuteCommand(new Commands.MoveCommand(triangle, -moveAmount, 0));
            }
            else if(keys == Keys.Right)
            {
                LevelEditor.UndoManager.ExecuteCommand(new Commands.MoveCommand(triangle, moveAmount, 0));
            }
        }
    }
}
