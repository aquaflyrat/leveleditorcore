﻿namespace PolyKnights.LevelEditor.Transform
{
    public class PolyFlipInPlace
    {
        public void Flip(ref Polygon triangle, int x1, int y1, int x2, int y2, int x3, int y3)
        {
            triangle.Points[0].X += x1;
            triangle.Points[0].Y += y1;
            triangle.Points[1].X += x2;
            triangle.Points[1].Y += y2;
            triangle.Points[2].X += x3;
            triangle.Points[2].Y += y3;
        }

        private int[,] FlipArrayMatrix =
        {
            { 0, -64,  -128, 0,  0,    0},
            { 0,  0,    0,   0,  128,  0},
            { 0,  0,    0,   0,  0,   -128},
            { 0,  0,    0,   0, -128,  0}
        };  

        public void Flip(ref Polygon triangle)
        {
            int currentIndex = (int) triangle.Angle;

            if (triangle.IsFlipped)
            {
                Flip(ref triangle,
                    FlipArrayMatrix[currentIndex, 0],
                    FlipArrayMatrix[currentIndex, 1],
                    FlipArrayMatrix[currentIndex, 2],
                    FlipArrayMatrix[currentIndex, 3],
                    FlipArrayMatrix[currentIndex, 4],
                    FlipArrayMatrix[currentIndex, 5]);
            } else
            {
                Flip(ref triangle,
                    -FlipArrayMatrix[currentIndex, 0],
                    -FlipArrayMatrix[currentIndex, 1],
                    -FlipArrayMatrix[currentIndex, 2],
                    -FlipArrayMatrix[currentIndex, 3],
                    -FlipArrayMatrix[currentIndex, 4],
                    -FlipArrayMatrix[currentIndex, 5]);
            }
        }
    }
}
