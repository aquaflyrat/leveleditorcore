﻿namespace PolyKnights.LevelEditor.Transform
{
    public class PolyRotateTransform
    {
        // { 0X, 0Y, 1X, 1Y, 2X, 2Y }
        private int[][] NonFlippedRotationsMatrix = new int[][]{
            new int[] {  128,  128, 0, 0, -64,  64   },
            new int[] {  128, -128, 0, 0,  64,   64  },
            new int[] { -128, -128, 0, 0,  64,  -64  },
            new int[] { -128,  128, 0, 0, -64, -64   }
        };

        private int[][] FlippedRotationsMatrix = {
            new int [] { 0, 0, 0, -64, -128, -128},
            new int [] { },
            new int [] { },
            new int [] { }
        };
        
        public void Rotate(ref Polygon triangle, int[] array)
        {
            triangle.Points[0].X += array[0];
            triangle.Points[0].Y += array[1];
            triangle.Points[1].X += array[2];
            triangle.Points[1].Y += array[3];
            triangle.Points[2].X += array[4];
            triangle.Points[2].Y += array[5];
        }

        public Angle GetPreviousAngle(Polygon triangle)
        {
            int index = (int)triangle.Angle;
            if (index == 0)
                return Angle.ThreeSixty;
            else
                return (Angle)(index - 1);

        }

        public Angle GetNextAngle(Polygon triangle)
        {
            int index = (int)triangle.Angle;
            if (index == 3)
                return Angle.Zero;
            else
                return (Angle)(index + 1);
        }

        public void Rotate(ref Polygon triangle)
        {
            int currentIndex = (int) triangle.Angle;
            int[][] rotationArray = NonFlippedRotationsMatrix;

            Rotate(ref triangle,
                rotationArray[currentIndex]
            );

            if (currentIndex == 3)
            {
                triangle.Angle = Angle.Zero;
            }
            else
            {
                currentIndex++;
                triangle.Angle = (Angle)currentIndex;
            }
        }

        public void Rotate(ref Polygon triangle, Angle angle)
        {
            // Don't try to handle flipp'in triangles... Yet
            // ToDo: Handle Flipped triangles
            int currentIndex = (int) angle;

            int[][] rotationArray = triangle.IsFlipped ? FlippedRotationsMatrix : NonFlippedRotationsMatrix;

            Rotate(ref triangle,
                rotationArray[currentIndex]
            );
            
        }
    }
}
