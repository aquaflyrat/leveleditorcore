﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyKnights.LevelEditor
{
    public enum EditState { Place, Select, Wireframe, None };
    public static class EditMode
    {
        public static EditState State = EditState.Place;

        public static string AsString()
        {
            return State.ToString();
        }
    }
}
