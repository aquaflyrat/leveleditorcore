﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyKnights.LevelEditor.Sprites
{
    public class SpriteInfo
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }
}
