﻿using System.Drawing;
using System.Collections.Generic;

namespace PolyKnights.LevelEditor
{
    // TODO: Refactor this into a singleton
    public class Canvas
    {
        private static List<Polygon> Polygons = new List<Polygon>();
        private static List<Sprite> Sprites = new List<Sprite>();

        public List<GameObject> GetAllObjects()
        {
            List<GameObject> objects = new List<GameObject>();
            objects.AddRange(Polygons);
            objects.AddRange(Sprites);
            return objects;
        }

        public bool AddPolygon(Polygon obj)
        {
            if (Polygons.Contains(obj))
                return false;

            Polygons.Add(obj);
            return true;
        }
        
        public void AddSprite(Sprite sprite)
        {
            // TODO: Check for duplicate positioned objects
            if (Sprites.Contains(sprite))
                return;

            Sprites.Add(sprite);
        }

        public List<Polygon> GetPolygons()
        {
            return Polygons;
        }
        
        public void RemovePolygon(Polygon obj)
        {
            if (!Polygons.Contains(obj))
                return;

            Polygons.Remove(obj);
        }

        public void RemoveSprite(Sprite sprite)
        {
            if (!Sprites.Contains(sprite))
                return;

            Sprites.Remove(sprite);
        }

        public void RemoveObject(GameObject obj)
        {
            if (obj.Type == ObjectType.Polygon)
                RemovePolygon((Polygon) obj);
            else if (obj.Type == ObjectType.Image)
                RemoveSprite((Sprite) obj);
        }

        public void AddObject(GameObject obj)
        {
            if (obj.Type == ObjectType.Polygon)
                AddPolygon((Polygon) obj);
            else if (obj.Type == ObjectType.Image)
                AddSprite((Sprite) obj);
        }

        public void Clear()
        {
            Polygons.Clear();
        }

        public void Move(int dx, int dy)
        {
            Transform.TranslationTransform translation = new Transform.TranslationTransform();
            translation.MoveTriangles(Polygons, dx, dy);
        }

        public void Scale(int sx, int sy)
        {

        }

        public void Draw(Graphics g)
        {
            foreach(Polygon obj in Polygons)
            {
                obj.Draw(g);
            }

            foreach (Sprite obj in Sprites)
            {
                obj.Draw(g);
            }

        }
    }
}
