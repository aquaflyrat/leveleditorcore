﻿using System.Drawing;

namespace PolyKnights.LevelEditor
{
    public class Mouse
    {
        private Point point = new Point();
        private Point lastPoint = new Point();

        public int X { get { return point.X; } private set { point.X = value; } }
        public int Y { get { return point.Y; } private set { point.Y = value; } }

        public void SetPosition(int x, int y)
        {
            point.X = x;
            point.Y = y;
            
            lastPoint = point;
        }

        public Point Point()
        {
            return point;
        }
    }
}
