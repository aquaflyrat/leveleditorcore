﻿using System.Drawing;

namespace PolyKnights.LevelEditor.Settings
{
    public class AppSettings
    {
        private static void RunProperties()
        {
            Settings settings = new Settings();
            var group = settings.AddGroup("Run Properties");
            group.AddSetting("AppPath", "");
            group.AddSetting("CmdArgs", "");
            group.AddSetting("RunWithDefaultArgs", true);
        }

        private static void PolygonSettings()
        {
            Settings settings = new Settings();
            var group = settings.AddGroup("Polygon Properties");
            group.AddSetting("Base", 128);
            group.AddSetting("Height", 64);
        }

        private static void MapProperties()
        {
            Settings settings = new Settings();
            var group = settings.AddGroup("Map Properties");

            group.AddSetting("PolygonMoveDistance", 64);

            group.AddSetting("GridWidth", 64);
            group.AddSetting("GridHeight", 64);
            group.AddSetting("GridColorAlpha", 75);
            group.AddSetting("GridColor", Color.FromArgb(75, 211, 211, 211));

            group.AddSetting("PlaceSnapX", 128);
            group.AddSetting("PlaceSnapY", 64);
            group.AddSetting("PointSnapX", 32);
            group.AddSetting("PointSnapY", 32);
        }

        private static void EditorSettings()
        {
            Settings settings = new Settings();
            var group = settings.AddGroup("Editor Settings");
            group.AddSetting("OpenTopOnLoad", false);
        }

        public static void RecentFiles()
        {
            Settings settings = new Settings();
            var group = settings.AddGroup("Recent Files");
        }

        public static void Initalize()
        {
            MapProperties();
            PolygonSettings();
            RecentFiles();
            RunProperties();
            EditorSettings();
            
            if (System.IO.File.Exists(Settings.FileName))
            {
                Settings.OverrideNessersary();
            }

        }

    }
}
