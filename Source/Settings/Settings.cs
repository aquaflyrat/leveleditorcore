﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace PolyKnights.LevelEditor.Settings
{
    [System.Serializable]
    public struct Setting
    {
        public object Value;
        public object DefaultValue;
    }

    [System.Serializable]
    public class SettingGroup
    {
        public string Name;
        private Dictionary<string, Setting> settings = new Dictionary<string, Setting>();
        private Dictionary<string, SettingGroup> groups = new Dictionary<string, SettingGroup>();

        public void AddSetting<T>(string name, T value, T default_)
        {
            foreach (var pair in settings)
                if (pair.Key == name) return;

            Setting setting = new Setting();
            setting.Value = value;
            setting.DefaultValue = default_;
            settings.Add(name, setting);
        }

        public void Clear()
        {
            settings.Clear();
            groups.Clear();
        }

        public void AddSetting<T>(string name, T value)
        {
            AddSetting(name, value, value);
        }

        public SettingGroup AddGroup(string name)
        {
            SettingGroup group = new SettingGroup();
            group.Name = name;
            groups.Add(name, group);
            return group;
        }

        public object GetValue(string name)
        {
            Setting tmp;
            settings.TryGetValue(name, out tmp);
            return tmp.Value;
        }

        public T GetValue<T>(string name)
        {
            Setting tmp;
            settings.TryGetValue(name, out tmp);
            return (T)tmp.Value;
        }

        public void SetValue<T>(string name, T value)
        {
            Setting setting = new Setting();
            setting.Value = value;
            setting.DefaultValue = settings[name].DefaultValue;
            settings[name] = setting;
        }

        public object GetDefault(string name)
        {
            Setting tmp;
            settings.TryGetValue(name, out tmp);
            return tmp.DefaultValue;
        }

        public void ResetToDefault()
        {
            foreach(var key in settings.Keys.ToList())
            {
                Setting tmp = new Setting();
                tmp.Value = settings[key].DefaultValue;
                tmp.DefaultValue = settings[key].DefaultValue;
                settings[key] = tmp;
            }

            foreach(var pair in groups)
            {
                pair.Value.ResetToDefault();
            }
        }

        public List<Setting> GetAll()
        {
            return settings.Values.ToList();
        }

        public List<T> GetAllValues<T>()
        {
            List<T> list = new List<T>();
            foreach(var setting in GetAll())
            {
                list.Add((T)setting.Value);
            }

            return list;
        }

        public void Debug()
        {
            LevelEditor.UserError("[Root]");
            foreach(var s in settings)
            {
                LevelEditor.UserError("\t- " + s.Key + " = " + s.Value.Value);
            }

            foreach(var group in groups)
            {
                LevelEditor.UserError("\t[" + group.Key + "]");
                foreach(var s in group.Value.settings)
                {
                    LevelEditor.UserError("\t\t- " + s.Key + " = " + s.Value.Value);
                }
            }
        }
    }
    
    [System.Serializable]
    public class Settings
    {
        private static Dictionary<string, SettingGroup> settings = new Dictionary<string, SettingGroup>();
        public static string FileName = "editor.config";
           
        public SettingGroup AddGroup(string name)
        {
            SettingGroup group = new SettingGroup();
            group.Name = name;
            settings.Add(name, group);
            return group;
        }

        public SettingGroup GetGroup(string name)
        {
            SettingGroup tmp = null;
            settings.TryGetValue(name, out tmp);
            return tmp;
        }

        public static void OverrideNessersary()
        {
            var dict = Load();
            settings = dict;
        }

        [System.Serializable]
        private class TmpSettings
        {
            public Dictionary<string, SettingGroup> Settings = new Dictionary<string, SettingGroup>();
        }
        
        public static void Save()
        {
            TmpSettings tmp = new TmpSettings();
            tmp.Settings = settings;
            BinaryFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, tmp);
            stream.Close();
        }

        public static Dictionary<string, SettingGroup> Load()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            TmpSettings obj = (TmpSettings)formatter.Deserialize(stream);
            stream.Close();
            return obj.Settings;
        }
    }
}
