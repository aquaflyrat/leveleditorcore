﻿namespace PolyKnights.LevelEditor
{
    public class About
    {
        public struct Revision
        {
            public static int Major = 0;
            public static int Minor = 9;
            public static int Patch = 8;
            public static string Build = "0000926 (Pre-Release)";

            public static string Version = "Version: " + Major + "." + Minor + "." + Patch;
        }

        public static string License = 
        "\r\n\r\nMIT License\r\n" + 
        "Copyright(c) 2016\r\n" +
        "Barney Wilks\r\n" +
        "Permission is hereby granted, free of charge, to any person obtaining a copy" +
        "of this software and associated documentation files(the 'Software'), to deal" +
        "in the Software without restriction, including without limitation the rights" +
        "to use, copy, modify, merge, publish, distribute, sublicense, and/or sell" +
        "copies of the Software, and to permit persons to whom the Software is" +
        "furnished to do so, subject to the following conditions:" +
        "The above copyright notice and this permission notice shall be included in all" +
        "copies or substantial portions of the Software.\r\n\r\n" +
        "THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR" +
        "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY," +
        "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE" +
        "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER" +
        "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM," +
        "OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE" +
        "SOFTWARE.";

        public static string Author = "Barney Wilks © 2016";
        public static string Name = "F.A.R.T Engine";
        public static string Desc = "PolyKnights development toolkit.";

        public static string BugtrackerURL = "https://bitbucket.org/aquaflyrat/leveleditorcore/issues?status=new&status=open";

    }
}
