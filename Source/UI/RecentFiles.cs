﻿using System.Windows.Forms;
using System;

namespace PolyKnights.LevelEditor.UI
{
    public class RecentFiles
    {
        private const int maxRecentFiles = 6;

        public void AddFile(EventHandler onClick, ref ToolStripMenuItem recentFilesMenu, Toolbox.File currentFile, string recentFileName)
        {
            foreach (ToolStripMenuItem item in recentFilesMenu.DropDownItems)
            {
                if (item.Text == currentFile.FileName) return;
            }

            var settings = new Settings.Settings();
            var group = settings.GetGroup("Recent Files");
            group.AddSetting(recentFileName, recentFileName);

            Settings.Settings.Save();
            LoadAll(onClick, ref recentFilesMenu);
        }

        public void LoadAll(EventHandler onClick, ref ToolStripMenuItem recentFilesMenu)
        {
            recentFilesMenu.DropDownItems.Clear();
            var files = new Settings.Settings().GetGroup("Recent Files").GetAllValues<string>();
            foreach (var recentFile in files)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(recentFile);
                item.Click += onClick;
                recentFilesMenu.DropDownItems.Insert(0, item);
            }
         }
    }
}
