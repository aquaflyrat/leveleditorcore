﻿using System.Runtime.InteropServices;
using System.Diagnostics;

namespace PolyKnights.LevelEditor.UI
{
    public class ApplicationRunner
    {
        [DllImport("user32.dll")]
        internal static extern System.IntPtr SetForegroundWindow(System.IntPtr hWnd);

        [DllImport("user32.dll")]
        internal static extern bool ShowWindow(System.IntPtr hWnd, int nCmdShow);

        private static Process CurrentProcess = null;
        private const string ExeName = "app.exe";

        public void Start(System.Windows.Forms.Form parent, Toolbox.File currentFile)
        {
            Settings.Settings settings = new Settings.Settings();
            var group = settings.GetGroup("Run Properties");
            string path = group.GetValue<string>("AppPath");

            if(path == "")
            {
                new Forms.RunProperties().Show(parent);
            }

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WorkingDirectory = group.GetValue<string>("AppPath");
            startInfo.FileName = startInfo.WorkingDirectory + "/" + ExeName;
            startInfo.UseShellExecute = false;

            bool useDefaultArgs = group.GetValue<bool>("RunWithDefaultArgs");
            if(useDefaultArgs)
            {
                startInfo.Arguments = '"' + currentFile.FileName + '"';
            } else
            {
                startInfo.Arguments = group.GetValue<string>("CmdArgs");
            }

            if (currentFile == Toolbox.File.NewFile)
            {
                LevelEditor.UserError("Cannot open an unsaved map!");
                return;
            }

            CurrentProcess = new Process();
            CurrentProcess.StartInfo = startInfo;
            CurrentProcess.Start();
            BringToFront();
        }

        void DataRecived(object sender, DataReceivedEventArgs e)
        {
            LevelEditor.UserError(e.Data);
        }

        private void BringToFront()
        {
            System.IntPtr hWnd = CurrentProcess.MainWindowHandle;
            if (hWnd == System.IntPtr.Zero)
                return;

            SetForegroundWindow(hWnd);
            ShowWindow(hWnd, 0);
        }

        public void Stop()
        {
            if (CurrentProcess == null)
                return;

            if (CurrentProcess.ProcessName.Length == 0)
                return;

            CurrentProcess.Kill();
            CurrentProcess.WaitForExit();
        }

        public bool IsRunning()
        {
            if (CurrentProcess == null)
                return false;
            if (CurrentProcess.ProcessName.Length == 0)
                return false;

            return true;
        }
    }
}
