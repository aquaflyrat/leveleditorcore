﻿using System.Collections.Generic;
using System.Drawing;

namespace PolyKnights.LevelEditor.Toolbox
{
    public class FileSaver
    {
        public void SaveFile(string filePath, List<Polygon> list, Camera camera)
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(filePath))
            {
                var go = EditorArea.Grid.Position;
                file.WriteLine(list.Count);
                foreach (Polygon poly in list)
                {
                    SolidBrush brush = poly.Brush as SolidBrush;
                    Color color = brush.Color;
                    float r = color.R / 255.0f, g = color.G / 255.0f, b = color.B / 255.0f;
                    
                    int p1x = (int)poly.Points[0].X - camera.x;
                    int p1y = (int)poly.Points[0].Y - camera.y;

                    int p2x = (int)poly.Points[1].X - camera.x;
                    int p2y = (int)poly.Points[1].Y - camera.y;

                    int p3x = (int)poly.Points[2].X - camera.x;
                    int p3y = (int)poly.Points[2].Y - camera.y;
                    file.WriteLine(r + " " + g + " " + b + " " + p1x + " " + p1y + " " + p2x + " " + p2y + " " + p3x + " " + p3y);
                }

                foreach (Polygon poly in list)
                {
                    int isFlipped = (poly.IsFlipped) ? 1 : 0;
                    int isModified = 0; //(poly.IsModified) ? 1 : 0;
                    file.WriteLine(isFlipped + " " + isModified);
                }
            }
        }
    }
}
