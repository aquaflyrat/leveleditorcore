﻿using System.Collections.Generic;
using System.Drawing;

namespace PolyKnights.LevelEditor.Toolbox
{
    public class FileLoader
    {
        public void LoadFile(ref List<Polygon> outList, string fileName, Canvas canvas)
        {
            System.IO.StreamReader file;
            try {
                file =
                   new System.IO.StreamReader(fileName);
            } catch(System.IO.IOException)
            {
                file = new System.IO.StreamReader("../../" + fileName);
            }
            int nTriangles = 0;
            string line0 = file.ReadLine();
            int.TryParse(line0, out nTriangles);
            canvas.GetPolygons().Clear();
            int count = 0;
            string line = file.ReadLine();
            while (count <= nTriangles-1)
            {
                string[] currentLine = line.Split(' ');
                float fr, fg, fb;
                float.TryParse(currentLine[0], out fr);
                float.TryParse(currentLine[1], out fg);
                float.TryParse(currentLine[2], out fb);
                int r = (int) (fr * 255);
                int g = (int) (fg * 255);
                int b = (int) (fb * 255);
                Polygon triangle = new Polygon();
                triangle.Brush = new SolidBrush(Color.FromArgb(255, r, g, b));

                int p1x, p1y;
                int p2x, p2y;
                int p3x, p3y;
                int.TryParse(currentLine[3], out p1x);
                int.TryParse(currentLine[4], out p1y);
                int.TryParse(currentLine[5], out p2x);
                int.TryParse(currentLine[6], out p2y);
                int.TryParse(currentLine[7], out p3x);
                int.TryParse(currentLine[8], out p3y);

                Point p1 = new Point(p1x, p1y);
                Point p2 = new Point(p2x, p2y);
                Point p3 = new Point(p3x, p3y);

                triangle.Points = new PointF[] { p1, p2, p3 };
                canvas.AddPolygon(triangle);

                line = file.ReadLine();
                count++;
            }
            int i = 0;
            while (line != null)
            {
                var poly = canvas.GetPolygons()[i];
                string[] currentLine = line.Split(' ');
                int flipped;
                int modified;
                int.TryParse(currentLine[0], out flipped);
                int.TryParse(currentLine[1], out modified);
                poly.IsFlipped = flipped != 0;
                i++;
                line = file.ReadLine();
            }
        }
    }
}
