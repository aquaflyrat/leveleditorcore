﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PolyKnights.LevelEditor.Toolbox.Extensions
{
    public static class Drawing
    {
        public static void DrawCircle(this Graphics g, Pen pen, int thickness,  PointF location, int radius)
        {
            using (Pen p2 = new Pen(pen.Brush, thickness))
            {
                int x = (int)location.X-radius;
                int y = (int)location.Y-radius;
                g.DrawEllipse(p2, x, y, radius * 2, radius * 2);
            }
        }
    }
}
