﻿using System.Windows.Forms;

namespace PolyKnights.LevelEditor.Toolbox
{
    public class File
    {
        public string FileName;
        public bool IsSaved;

        public File(string fileName, bool saved=true)
        {
            this.FileName = fileName;
            this.IsSaved = saved;
        }

        public static File NewFile = new File("New File", true);
    }
}
