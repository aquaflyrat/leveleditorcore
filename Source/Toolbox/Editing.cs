﻿using System.Collections.Generic;
using System.Drawing.Imaging;

namespace PolyKnights.LevelEditor.Toolbox
{
    public class Editing
    {
        public static Polygon GetPolygonAtMouse(List<Polygon> polys, Mouse mouse)
        {
            for (int i = 0; i < polys.Count; i++)
            {
                var poly = polys[i];
                if (Toolbox.Maths.PointInsidePolygon(mouse.Point(), poly))
                {
                    return poly;
                }
            }

            return null;
        }

        public static void DrawImage(System.Drawing.Graphics g, ref System.Drawing.Bitmap bitmap, float opacity, int x, int y)
        {
            ColorMatrix colMatrix = new ColorMatrix();
            colMatrix.Matrix33 = opacity;
            ImageAttributes attribs = new ImageAttributes();
            attribs.SetColorMatrix(colMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            g.DrawImage(bitmap, new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), x, y, bitmap.Width, bitmap.Height, System.Drawing.GraphicsUnit.Pixel, attribs);
        }
    }
}
