﻿using System;
using System.Drawing;

namespace PolyKnights.LevelEditor.Toolbox
{
    public class Maths
    {
        public static float Sign(PointF p1, PointF p2, PointF p3)
        {
            return (p1.X - p3.X) * (p2.Y - p3.Y) - (p2.X - p3.X) * (p1.Y - p3.Y);
        }

        public static bool PointInsidePolygon(Point point, Polygon triangle)
        {
            bool b1, b2, b3;
            b1 = Sign(point, triangle.Points[0], triangle.Points[1]) < 0.0f;
            b2 = Sign(point, triangle.Points[1], triangle.Points[2]) < 0.0f;
            b3 = Sign(point, triangle.Points[2], triangle.Points[0]) < 0.0f;

            return ((b1 == b2) && (b2 == b3));
        }

        public static bool PointInsideCircle(PointF position, PointF centre, int radius)
        {
            bool inside = Math.Sqrt((position.X - centre.X) * (position.X - centre.X) + (position.Y - centre.Y) * (position.Y - centre.Y)) < radius;
            return inside;
        }

        public static bool PolygonInsideRect(Polygon triangle, Rectangle rect)
        {
            return rect.Contains(Point.Round(triangle.Points[0])) || rect.Contains(Point.Round(triangle.Points[1])) || rect.Contains(Point.Round(triangle.Points[2]));
        }
    }
}
