﻿using System;
using System.Windows.Forms;

namespace PolyKnights.LevelEditor
{
   public  static class Program
    {
        public static LevelEditor Editor;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Editor = new LevelEditor();
            Application.Run(Editor);
        }
    }
}
