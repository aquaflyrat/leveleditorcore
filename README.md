# README #


### What is this repository for? ###

* The Level Editor for the game code named F.A.R.T or PolyKnights depending on who you ask.

### How do I get set up? ###

* The project should run out the box, with the .csproj being included.
* Currently has not been tested under Mono, and only on Windows up .NET.

### Contribution guidelines ###

* N/A

### Who do I talk to? ###

* blaclegi (Main Editor Dev)
* HolyBlackCat (Does the game/engine, and pretty much everything else).